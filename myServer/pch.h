﻿// pch.h: 这是预编译标头文件。
// 下方列出的文件仅编译一次，提高了将来生成的生成性能。
// 这还将影响 IntelliSense 性能，包括代码完成和许多代码浏览功能。
// 但是，如果此处列出的文件中的任何一个在生成之间有更新，它们全部都将被重新编译。
// 请勿在此处添加要频繁更新的文件，这将使得性能优势无效。

#ifndef PCH_H
#define PCH_H

// 添加要在此处预编译的标头
#include "framework.h"


//-----------------------------------------写在这里 别的地方用这些头文件中的函数,不需要include了-------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------->>>
//MYSDK.H包含 现在全部移动到这里
#include <windows.h>
#include <Psapi.h>
#include <TlHelp32.h>
#include <optional>
#include <string>
#include <vector>
#include <queue>
#include <sstream>
#include <algorithm>
#include <random>
#include <memory>
#include <thread>
#include <codecvt>
#include <filesystem>
//必须在FMT文件夹中 否则文件名冲突 附加包含目录在这个文件夹上层即可
#include <fmt/chrono.h>
#include <fmt/color.h>
#include <fmt/core.h>
#include <fmt/os.h>
#include <fmt/ranges.h>

/////////////////////////////MySdk.H放 最后 因为内存池他用到了  所以这里三方库都放在最前面/////////////////////////////////////////////
//内存池 /DNOMINMAX=1 C++编译的命令行加入 不然max函数冲突
#include <foonathan/memory/container.hpp>   // vector, list, list_node_size,...
#include <foonathan/memory/memory_pool.hpp> // memory_pool
// alias namespace foonathan::memory as memory for easier access
#include <foonathan/memory/namespace_alias.hpp>
using namespace memory::literals;
/*全局初始化一个内存池  https://github.com/foonathan/memory/blob/main/example/using_allocators.cpp
memory::memory_pool<> pool(memory::list_node_size<int>::value, 4_KiB);*/
#define FPrintf fmt::print
#define FFormat fmt::format
#include "httplib.h"
//--------------->msgpack必须声明MSGPACK_NO_BOOST 不然会使用BOOST库
#define  MSGPACK_NO_BOOST
#include "msgpack.hpp"
//--------------->内存加载
#include "MemoryModule.h"
//--------------->内存加载 keystone
#include "keystone/KeystoneMemory.h"
//--------------->内存加载 capstone
#include "capstone/include/capstone/capstone.h"
////////////////////////////////////自己的//////////////////////////////////////
#include "MySdk.h"


//<<<----------------------------------------------------









#endif //PCH_H
