# ErGouSdk

#### 使用说明
全部函数在MySdk.h文件中。全部实现在MySdk.cpp中。
不要被MyServer名称迷惑,这个是我工程乱取名。
使用的三方库在pch.h中包含的
#### 主要文件
	你只需要看 MySdk.h  MySdk.cpp pch.h myServer.cpp就行,可以搬出来自己用

例子在 myServer.cpp  未写全例子,自己可以看看。
### 工程使用MD模式编译
#### 三方库(不自己安装跳过,我已经把lib和头文件丢进来了)
### 1.msgpack-c-cpp 
要自己去git,我这里已经打包了(在 "使用到的三方库"目录),这个库不用编译包含文件就行:
	https://github.com/msgpack/msgpack-c/releases/tag/cpp-6.0.0
	使用msgpack:
		必须定义  #define  MSGPACK_NO_BOOST
		然后		 #include "msgpack.hpp"
		我已经在pch.h弄好了
### 2.foonathan-memory(内存池)
使用vcpkg安装(我是自己编译的,不是DLL版本)
	C:\vcpkg-master\vcpkg.exe install  foonathan-memory  安装了把lib、include的文件提取就行了 这个库没dll版本

### 反汇编引擎 capstone:
	C:\vcpkg-master\vcpkg.exe install  capstone 经过我测试这个DLL不支持任何汇编指令,
	是个垃圾玩意,然后重新从github下载
	https://github.com/capstone-engine/capstone/releases/tag/4.0.2
	有静态库,没必要内存加载了,EXE体积直接+3兆,VS2019可以用,
	至于是MD还是MT模式的,鬼知道了,工程就是用的就是这款,
	不放心可以下载链接的源码包自己编译。
	他也有动态库版本,我怀疑改造不了内存加载,试过,
	提示什么指令集不支持,估计内存加载缺水了啥操作.将就用吧。
	他的兄弟  keystone汇编引擎 也是可以vcpkg安装,我以前已经改造过他了


### 乾坤大挪移 崩溃解释:
	因为代码中要读取原始函数的指定长度字节,
	写太大可能会读无效内存,所以实际使用要避免下了,
	一般情况下不会遇到那么多这种状况