#include "pch.h"
#include "mySdk.h"
#include "httplib.h"
using namespace std;
namespace MySdk {
	inline bool ISKEYDOWN(DWORD vk_code) {
		return ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0);
	}

	inline bool ISKEYUP(DWORD vk_code) {
		return ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1);
	}



	inline void us_Delay(ULONG ulMicroSeconds)
	{
		LARGE_INTEGER  timeStop;
		LARGE_INTEGER  timeStart;
		LARGE_INTEGER  Freq;
		LONGLONG  ulTimeToWait;

		if (!QueryPerformanceFrequency(&Freq))
			return;

		QueryPerformanceCounter(&timeStart);

		ulTimeToWait = Freq.QuadPart * ulMicroSeconds / 1000 / 1000;

		timeStop = timeStart;

		while ((timeStop.QuadPart - timeStart.QuadPart) < ulTimeToWait)
		{
			QueryPerformanceCounter(&timeStop);
		}
	}



	int RtlRandomEx(int a, int b)
	{
		static ULONG seed = 1;//这个是传入的指针,每次_RtlRandomEx后是会被改的

		typedef ULONG(__stdcall* _RtlRandomEx)(
			PULONG Seed
			);
		static _RtlRandomEx m_RtlRandomEx = (_RtlRandomEx)GetProcAddress(GetModuleHandle("ntdll"), "RtlRandomEx");
		return a + m_RtlRandomEx(&seed) % (b - a + 1);
	}

	void ErrorOut(const char* fmt, ...) {
		char szMsg[1024 * 4] = { 0 };
		vsprintf_s(szMsg, 1024 * 4, fmt, (char*)((&fmt) + 1));
		::MessageBox(NULL, szMsg, "二狗提示",
			MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);
	}

	void ErrorOutExit(const char* fmt, ...) {
		char szMsg[1024 * 4] = { 0 };
		vsprintf_s(szMsg, 1024 * 4, fmt, (char*)((&fmt) + 1));
		::MessageBox(NULL, szMsg, "二狗提示",
			MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);
		::ExitProcess(0); // exit会调用析构函数  ExitProcess是立即结束
	}

	DWORD ErrorOutExitYesNo(const char* fmt, ...) {

		char szMsg[1024];
		vsprintf_s(szMsg, 1024, fmt, (char*)((&fmt) + 1));
		return ::MessageBox(NULL, szMsg, "二狗提示",
			MB_YESNO | MB_ICONERROR | MB_SYSTEMMODAL);

	}


	int MessageBoxTimeoutA(HWND hWnd, LPCSTR lpText,
		LPCSTR lpCaption, UINT uType, WORD wLanguageId,
		DWORD dwMilliseconds)
	{

		typedef int(__stdcall* MSGBOXAAPI)(IN HWND hWnd,
			IN LPCSTR lpText, IN LPCSTR lpCaption,
			IN UINT uType, IN WORD wLanguageId, IN DWORD dwMilliseconds);

		static MSGBOXAAPI MsgBoxTOA = NULL;

		if (!MsgBoxTOA)
		{
			HMODULE hUser32 = GetModuleHandle(_T("user32.dll"));
			if (hUser32)
			{
				MsgBoxTOA = (MSGBOXAAPI)GetProcAddress(hUser32,
					"MessageBoxTimeoutA");
				//fall through to 'if (MsgBoxTOA)...'
			}
			else
			{
				//stuff happened, add code to handle it here 
				//(possibly just call MessageBox())
				return 0;
			}
		}

		if (MsgBoxTOA)
		{
			return MsgBoxTOA(hWnd, lpText, lpCaption,
				uType, wLanguageId, dwMilliseconds);
		}

		return 0;
	}


	void 删除开机启动()
	{
		//添加以下代码
		HKEY   hKey;
		char pFileName[MAX_PATH] = { 0 };
		//得到程序自身的全路径 
		DWORD dwRet = GetModuleFileName(NULL, pFileName, MAX_PATH);
		//找到系统的启动项 
		const char* lpRun = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
		//打开启动项Key 
		{
			long lRet = RegOpenKeyEx(HKEY_CURRENT_USER, lpRun, 0, KEY_ALL_ACCESS, &hKey);
			if (lRet == ERROR_SUCCESS)
			{
				//删除注册
				RegDeleteValue(hKey, "UilibDemo");
				RegCloseKey(hKey);
			}
		}

		{
			long lRet = RegOpenKeyEx(HKEY_LOCAL_MACHINE, lpRun, 0, KEY_ALL_ACCESS, &hKey);
			if (lRet == ERROR_SUCCESS)
			{
				//删除注册
				RegDeleteValue(hKey, "UilibDemo");
				RegCloseKey(hKey);
			}
		}
	}


	void 添加开机启动(std::string& path)
	{
		//添加以下代码
		HKEY   hKey;
		//找到系统的启动项 
		const char* lpRun = "Software\\Microsoft\\Windows\\CurrentVersion\\Run";
		//打开启动项Key 
		long lRet = RegOpenKeyEx(HKEY_CURRENT_USER, lpRun, 0, KEY_ALL_ACCESS, &hKey);
		if (lRet == ERROR_SUCCESS)
		{
			//添加注册
			RegSetValueEx(hKey, "UilibDemo", 0, REG_SZ, (const BYTE*)path.c_str(), path.size());
			RegCloseKey(hKey);
		}
		else {
			AfxMessageBox("打开注册表失败");
		}

		//MessaageSuper("%s %s", lpRun,path);
	}

	bool 重启机器()
	{
		HANDLE	hToken;
		TOKEN_PRIVILEGES tkp = {};//获取系统信息
		/*OSVERSIONINFO osvi;
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		if (GetVersionExA(&osvi) == 0) return FALSE;*/
		if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) return false;
		LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
		tkp.PrivilegeCount = 1;
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);
		ExitWindowsEx(EWX_REBOOT | EWX_FORCEIFHUNG, 0);
		return true;
	}

	inline unsigned int  crc32_code(const  char* buf, unsigned int size) {

		static const unsigned int crc32tab[] = {
			0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL,
			0x076dc419L, 0x706af48fL, 0xe963a535L, 0x9e6495a3L,
			0x0edb8832L, 0x79dcb8a4L, 0xe0d5e91eL, 0x97d2d988L,
			0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L, 0x90bf1d91L,
			0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
			0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L,
			0x136c9856L, 0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL,
			0x14015c4fL, 0x63066cd9L, 0xfa0f3d63L, 0x8d080df5L,
			0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L, 0xa2677172L,
			0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
			0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L,
			0x32d86ce3L, 0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L,
			0x26d930acL, 0x51de003aL, 0xc8d75180L, 0xbfd06116L,
			0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L, 0xb8bda50fL,
			0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
			0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL,
			0x76dc4190L, 0x01db7106L, 0x98d220bcL, 0xefd5102aL,
			0x71b18589L, 0x06b6b51fL, 0x9fbfe4a5L, 0xe8b8d433L,
			0x7807c9a2L, 0x0f00f934L, 0x9609a88eL, 0xe10e9818L,
			0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
			0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL,
			0x6c0695edL, 0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L,
			0x65b0d9c6L, 0x12b7e950L, 0x8bbeb8eaL, 0xfcb9887cL,
			0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L, 0xfbd44c65L,
			0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
			0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL,
			0x4369e96aL, 0x346ed9fcL, 0xad678846L, 0xda60b8d0L,
			0x44042d73L, 0x33031de5L, 0xaa0a4c5fL, 0xdd0d7cc9L,
			0x5005713cL, 0x270241aaL, 0xbe0b1010L, 0xc90c2086L,
			0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
			0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L,
			0x59b33d17L, 0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL,
			0xedb88320L, 0x9abfb3b6L, 0x03b6e20cL, 0x74b1d29aL,
			0xead54739L, 0x9dd277afL, 0x04db2615L, 0x73dc1683L,
			0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
			0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L,
			0xf00f9344L, 0x8708a3d2L, 0x1e01f268L, 0x6906c2feL,
			0xf762575dL, 0x806567cbL, 0x196c3671L, 0x6e6b06e7L,
			0xfed41b76L, 0x89d32be0L, 0x10da7a5aL, 0x67dd4accL,
			0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
			0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L,
			0xd1bb67f1L, 0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL,
			0xd80d2bdaL, 0xaf0a1b4cL, 0x36034af6L, 0x41047a60L,
			0xdf60efc3L, 0xa867df55L, 0x316e8eefL, 0x4669be79L,
			0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
			0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL,
			0xc5ba3bbeL, 0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L,
			0xc2d7ffa7L, 0xb5d0cf31L, 0x2cd99e8bL, 0x5bdeae1dL,
			0x9b64c2b0L, 0xec63f226L, 0x756aa39cL, 0x026d930aL,
			0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
			0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L,
			0x92d28e9bL, 0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L,
			0x86d3d2d4L, 0xf1d4e242L, 0x68ddb3f8L, 0x1fda836eL,
			0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L, 0x18b74777L,
			0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
			0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L,
			0xa00ae278L, 0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L,
			0xa7672661L, 0xd06016f7L, 0x4969474dL, 0x3e6e77dbL,
			0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L, 0x37d83bf0L,
			0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
			0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L,
			0xbad03605L, 0xcdd70693L, 0x54de5729L, 0x23d967bfL,
			0xb3667a2eL, 0xc4614ab8L, 0x5d681b02L, 0x2a6f2b94L,
			0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL, 0x2d02ef8dL
		};
		unsigned int i, crc;
		crc = 0xFFFFFFFF;


		for (i = 0; i < size; i++)
			crc = crc32tab[(crc ^ buf[i]) & 0xff] ^ (crc >> 8);

		return crc ^ 0xFFFFFFFF;
	}
	//转换
	memory::string<memory::new_allocator> memoryVectorToString(memory::vector<UCHAR, memory::new_allocator>& dt) {
		dt.push_back(0);
		memory::string<memory::new_allocator> s((const char*)dt.data());
		return s;
	}

	std::string  VectorToString(std::vector<UCHAR>& dt) {
		dt.push_back(0);
		std::string  s((const char*)dt.data());
		return s;
	}
}


namespace MySdk::Code_Encry {
	// protuBuf ZigZag编码 32位
	// https://blog.csdn.net/daaikuaichuan/article/details/105639884
	// https://blog.csdn.net/daaikuaichuan/article/details/105639884
	inline int int_to_zigzag(int n) { return (n << 1) ^ (n >> 31); }

	inline int zigzag_to_int(int n) {
		return (((unsigned int)n) >> 1) ^ -(n & 1);
	}

	inline int64_t int64_to_zigzag(int64_t n) { return (n << 1) ^ (n >> 63); }

	inline int64_t zigzag_to_int64(int64_t n) {
		return (((int64_t)n) >> 1) ^ -(n & 1);
	}

	inline void __fastcall xor_encrypt(BYTE* pbData, int nSize) {

		static int xorkey[] = {
			0x41, 0xB6, 0x7F, 0x58, 0x38, 0x0C, 0xF0, 0x2D,
			0x7B, 0x39, 0x08, 0xFE, 0x21, 0xBB, 0x41, 0x58,
		};
		BYTE byTemp = 0;
		if (pbData) {
			for (int i = 0; i < nSize; ++i) {
				pbData[i] = pbData[i] ^ byTemp + xorkey[i & 15];
				byTemp = pbData[i];
			}
		}
	}

	inline void __fastcall xor_decrypt(BYTE* pbData, int nSize) {

		//原文链接：https ://blog.csdn.net/u012156872/article/details/107052557/
		static int xorkey[] = {
			0x41, 0xB6, 0x7F, 0x58, 0x38, 0x0C, 0xF0, 0x2D,
			0x7B, 0x39, 0x08, 0xFE, 0x21, 0xBB, 0x41, 0x58,
		};
		BYTE byPrev = 0;
		BYTE byTemp = 0;

		if (pbData) {
			for (int i = 0; i < nSize; ++i) {
				byPrev = pbData[i];
				pbData[i] = pbData[i] ^ byTemp + xorkey[i & 15];
				byTemp = byPrev;
			}
		}
	}
}

namespace MySdk::http {

	std::optional<std::string> HttpDownLoadFile(std::string& ip, int port, std::string& url, bool fixByRoot, bool urlEncode)
	{
		std::string urlFix = url;
		if (fixByRoot)
		{
			if (url.at(0) == '.')// "./游戏配置/123.txt"  转为 "/root/游戏配置/123.txt"
			{
				urlFix = "/root" + url.substr(1, url.length());
			}
			else
			{
				urlFix = url;
			}
		}
		std::string retcoding = urlFix;
		if (urlEncode)
		{
			//新增编码转换 防止URL中文编码出错
			char* txt = (char*)urlFix.c_str();
			TxtCoding::GB2312ToUTF_8(retcoding, txt, strlen(txt));
			retcoding = httplib::detail::encode_url(retcoding);
			Str::ReplaceAll(retcoding, "%E0%82", "%C2");//卡诺萨城·旧世.bmp 的特殊符号
			//retcoding = TxtCoding::encode_url(retcoding).c_str();
		}

		httplib::Client cli(ip.c_str(), port);

		cli.set_keep_alive(true);
		cli.set_connection_timeout(300, 0); // 300 milliseconds
		cli.set_read_timeout(20, 0); // 5 seconds
		cli.set_write_timeout(20, 0); // 5 seconds
		FPrintf("URL:{}\n", retcoding);
		auto res = cli.Get(retcoding.c_str()/*"/root/hzcnjak.dll"*/, [&](uint64_t len, uint64_t total) {
			ConsoleConfig::colorGreen();
			printf("\r%s %lld / %lld bytes => %d%% 已下载\t",
				url.c_str(),
				len, total,
				(int)(len * 100 / total));

			int show_num = (int)((len) * 10 / total);
			for (int j = 1; j <= show_num; j++)
			{
				printf("█");
			}
			ConsoleConfig::colorFen();
			for (int j = 1; j <= 10 - show_num; j++)
			{
				printf("█");
			}
			ConsoleConfig::colorWhite();
			return true; // return 'false' if you want to cancel the request.
			}
		);
		if (res.error() == httplib::Error::Success && res->status != 404)
		{
			printf("\r\n\t下载 状态:%d, 总字节:%d \n", res->status, res->body.size());
			/*ret = res->body;*/
			cli.stop();
			return res->body;
		}
		else
		{
			if (res.error() == 0)
			{
				printf("下载错误 Status Error:%d\n", res.error());
			}
			cli.stop();
			return std::nullopt;
		}
	}



	void  SendMiaoMaMessage(string& str, string& miaoma)//微信 公众号喵码提醒
	{
		httplib::Client cli("http://miaotixing.com");
		httplib::Params params;
		params.emplace("id", miaoma);
		params.emplace("text", str);
		auto res = cli.Put("/trigger", params);
		cli.stop();
	}

	namespace TxtCoding { //编码转换 HTTPLIB这个detail里面还有一大堆功能函数 比如BASE64
		inline std::string encode_url(const std::string& s) {
			return httplib::detail::encode_url(s);
		}
		inline std::string decode_url(const std::string& s,
			bool convert_plus_to_space) {
			return httplib::detail::decode_url(s, convert_plus_to_space);
		}
		//下面不是来自HTTPLIB 有效性待观察

		inline void GB2312ToUTF_8(string& pOut, char* pText, int pLen) {
			char buf[4];
			memset(buf, 0, 4);

			pOut.clear();

			int i = 0;
			while (i < pLen) {
				//如果是英文直接复制就可以
				if (pText[i] >= 0) {
					char asciistr[2] = { 0 };
					asciistr[0] = (pText[i++]);
					pOut.append(asciistr);
				}
				else {
					WCHAR pbuffer;
					Gb2312ToUnicode(&pbuffer, pText + i);

					UnicodeToUTF_8(buf, &pbuffer);

					pOut.append(buf);

					i += 2;
				}
			}

			return;
		}
		inline void Gb2312ToUnicode(WCHAR* pOut, char* gbBuffer) {
			::MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, gbBuffer, 2, pOut, 1);
			return;
		}
		inline void UTF_8ToUnicode(WCHAR* pOut, char* pText) {
			char* uchar = (char*)pOut;

			uchar[1] = ((pText[0] & 0x0F) << 4) + ((pText[1] >> 2) & 0x0F);
			uchar[0] = ((pText[1] & 0x03) << 6) + (pText[2] & 0x3F);

			return;
		}
		inline void UnicodeToUTF_8(char* pOut, WCHAR* pText) {
			// 注意 WCHAR高低字的顺序,低字节在前，高字节在后
			char* pchar = (char*)pText;

			pOut[0] = (0xE0 | ((pchar[1] & 0xF0) >> 4));
			pOut[1] = (0x80 | ((pchar[1] & 0x0F) << 2)) + ((pchar[0] & 0xC0) >> 6);
			pOut[2] = (0x80 | (pchar[0] & 0x3F));

			return;
		}
	}
}



//STR
namespace Str {
	std::vector<BYTE> stringToHexVector(std::string str)
	{
		std::vector<BYTE> ret;
		auto cc = SplitBySpace(str);
		for (auto& var : cc)
		{
			ret.push_back(std::stoi(var, nullptr, 16));
		}
		return ret;
	}
	std::vector<std::string> SplitByGetLine(const std::string& str, const char seq, const bool trim_empty)
	{
		std::stringstream ss(str);
		std::string subString;
		vector<std::string> ret;
		while (getline(ss, subString, seq))
		{
			if (subString.empty())
			{
				continue;
			}
			if (trim_empty)
			{
				subString = Str::Trim(subString);
			}
			if (subString.empty())
			{
				continue;
			}
			ret.push_back(subString);
		}
		return ret;
	}

	std::vector<std::string> SplitByGetLine(const std::string& str, const bool trim_empty)
	{
		std::stringstream ss(str);
		std::string subString;
		vector<std::string> ret;
		while (getline(ss, subString))
		{
			if (subString.empty())
			{
				continue;
			}
			if (trim_empty)
			{
				subString = Str::Trim(subString);
			}
			if (subString.empty())
			{
				continue;
			}
			ret.push_back(subString);
		}
		return ret;
	}
	std::vector<std::string> Compact(const std::vector<std::string>& tokens) {
		std::vector<std::string> compacted;

		for (size_t i = 0; i < tokens.size(); ++i) {
			if (!tokens[i].empty()) {
				compacted.push_back(tokens[i]);
			}
		}

		return compacted;
	}
	std::vector<std::string> SplitBySpace(const std::string& str)
	{
		std::vector<std::string> rt;
		std::stringstream ss(str);
		std::string tmp;
		while (ss >> tmp)
		{
			rt.push_back(tmp);
		}
		return rt;
	}
	std::vector<std::string> Split2(const std::string& str, const std::string& delim, const bool trim_empty) {
		size_t pos, last_pos = 0, len;
		std::vector<std::string> tokens;

		while (true) {
			pos = str.find(delim, last_pos);
			if (pos == std::string::npos) {
				pos = str.size();
			}

			len = pos - last_pos;
			if (!trim_empty || len != 0) {
				tokens.push_back(str.substr(last_pos, len));
			}

			if (pos == str.size()) {
				break;
			}
			else {
				last_pos = pos + delim.size();
			}
		}

		return tokens;
	}

	std::string Join(const std::vector<std::string>& tokens, const std::string& delim, const bool trim_empty) {
		if (trim_empty) {
			return Join(Compact(tokens), delim, false);
		}
		else {
			std::stringstream ss;
			for (size_t i = 0; i < tokens.size() - 1; ++i) {
				ss << tokens[i] << delim;
			}
			ss << tokens[tokens.size() - 1];

			return ss.str();
		}
	}

	std::string Trim(const std::string& str) {

		std::string blank = "\r\n\t ";
		size_t begin = str.size(), end = 0;
		for (size_t i = 0; i < str.size(); ++i) {
			if (blank.find(str[i]) == std::string::npos) {
				begin = i;
				break;
			}
		}

		for (size_t i = str.size(); i > 0; --i) {
			if (blank.find(str[i - 1]) == std::string::npos) {
				end = i - 1;
				break;
			}
		}

		if (begin >= end) {
			return "";
		}
		else {
			return str.substr(begin, end - begin + 1);
		}
	}

	std::string Repeat(const std::string& str, unsigned int times) {
		std::stringstream ss;
		for (unsigned int i = 0; i < times; ++i) {
			ss << str;
		}
		return ss.str();
	}


	std::string ToUpper(const std::string& str) {
		std::string s(str);
		std::transform(s.begin(), s.end(), s.begin(), [](int n) { return tolower(n); });
		return s;
	}

	std::string ToLower(const std::string& str) {
		std::string s(str);
		std::transform(s.begin(), s.end(), s.begin(), [](int n) { return tolower(n); });
		return s;
	}


	std::string ReadFile(const std::string& filepath) {
		std::ifstream ifs(filepath.c_str());
		std::string content((std::istreambuf_iterator<char>(ifs)),
			(std::istreambuf_iterator<char>()));
		ifs.close();
		return content;
	}

	void WriteFile(const std::string& filepath, const std::string& content) {
		std::ofstream ofs(filepath.c_str(), std::ios_base::binary | ios::trunc);//必须BIN模式 不然一些文本的 \r\n会变成\r\r\n
		ofs << content;
		ofs.close();
		return;
	}




	ATL::CAtlString StringToCstring(std::string const& s)
	{
		return s.c_str();
	}
	//转换 传入char*  转换成UTF8 char*字符串
	std::string CStringToUTF8String(ATL::CAtlString cstr, int& len)
	{
		WCHAR* unicode = cstr.AllocSysString();
		len = WideCharToMultiByte(CP_UTF8, 0, unicode, -1, NULL, 0, NULL, NULL);
		char* szUtf8 = (char*)malloc(len + 1);
		if (szUtf8) {
			memset(szUtf8, 0, len + 1);
			WideCharToMultiByte(CP_UTF8, 0, unicode, -1, szUtf8, len, NULL, NULL);
			std::string result = szUtf8;
			free(szUtf8);
			return result;
		}
		return "";
	}

	std::wstring string2wstring(std::string str)
	{
		std::wstring result;
		//获取缓冲区大小，并申请空间，缓冲区大小按字符计算  
		int len = MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), NULL, 0);
		WCHAR* buffer = new WCHAR[len + 1];
		//多字节编码转换成宽字节编码  
		MultiByteToWideChar(CP_ACP, 0, str.c_str(), str.size(), buffer, len);
		buffer[len] = '\0';             //添加字符串结尾  
		//删除缓冲区并返回值  
		result.append(buffer);
		delete[] buffer;
		return result;
	}

	//将wstring转换成string  
	std::string wstring2string(std::wstring wstr)
	{
		ATL::CAtlStringW WS = wstr.c_str();
		ATL::CAtlStringA WS2;
		WS2 = WS;

		return WS2.GetBuffer(0);
	}
	std::string WcharToChar(const wchar_t* wp, size_t encode)
	{
		std::string str;
		int len = WideCharToMultiByte((UINT)encode, 0, wp, (int)wcslen(wp), NULL, 0, NULL, NULL);
		char* m_char = new char[len + 1];
		memset(m_char, 0, len + 1);
		WideCharToMultiByte((UINT)encode, 0, wp, (int)wcslen(wp), m_char, len, NULL, NULL);
		str = std::string(m_char);
		delete[]m_char;
		return str;
	}
	bool isNum(std::string str)
	{
		for (int i = 0; i < (int)str.size(); i++)
		{
			int tmp = (int)str[i];
			if (tmp >= 48 && tmp <= 57)
			{
				continue;
			}
			else
			{
				return false;
			}
		}
		return true;
	}

	std::string GetHex(PBYTE Data, ULONG dwBytes) {
		std::string xx = "0";
		for (ULONG i = 0; i < dwBytes; i += 16) {
			xx += string_sprintf("%.3x==>%0.8x: ", i, (DWORD)Data + i);

			for (ULONG j = 0; j < 16; j++) {
				if (i + j < dwBytes) {
					xx += string_sprintf("%.2x ", Data[i + j]);
				}
				else {
					xx += string_sprintf("?? ");
				}
			}

			for (ULONG j = 0; j < 16; j++) {
				if (i + j < dwBytes && Data[i + j] >= 0x20 && Data[i + j] <= 0x7e) {
					xx += string_sprintf("%c", Data[i + j]);
				}
				else {
					xx += string_sprintf(".");
				}
			}

			xx += string_sprintf("\n");
		}
		return xx;
	}
	//

	void ReplaceAll(std::string& strSource, const std::string strOld, const std::string strNew){
		int nPos = 0;
		while ((nPos = strSource.find(strOld, nPos)) != strSource.npos){
			strSource.replace(nPos, strOld.length(), strNew);
			nPos += strNew.length();
		}
	}
	void split(const std::string src, const std::string separator, std::vector<std::string>& dest){

		std::string str = src;
		std::string substring;
		std::string::size_type start = 0, index = 0;

		do
		{
			index = str.find_first_of(separator, start);
			if (index != string::npos)
			{
				substring = str.substr(start, index - start);
				dest.push_back(substring);
				start = str.find_first_not_of(separator, index);
				if (start == string::npos) return;
			}
		} while (index != string::npos);


		substring = str.substr(start);
		dest.push_back(substring);
	}

	memory::vector<int, memory::new_allocator> stringToVecInt(const std::string str)
	{
		//https://blog.csdn.net/xxxxian666/article/details/120335279
		union {
			char c[2];
			int  i;
		} convert = {};

		// 段位清零
		convert.i = 0;

		memory::vector<int, memory::new_allocator> vec = {};

		for (unsigned i = 0; i < str.length(); i++) {
			// GBK编码首字符大于0x80
			if ((unsigned)str[i] > 0x80) {
				// 利用union进行转化，注意是大端序
				convert.c[1] = str[i];
				convert.c[0] = str[i + 1];
				vec.push_back(convert.i);
				i++;
			}
			else
				// 小于0x80，为ASCII编码，一个字节
				vec.push_back(str[i]);
		}
		return vec;
	}
	std::vector<BYTE> split_HexByte(const std::string src, const std::string separator){
		std::vector<std::string>  TmpSplit;
		split(src, separator, TmpSplit);
		std::vector<BYTE> retArray;
		for (auto& var : TmpSplit){
			retArray.push_back((BYTE)std::stoul((char*)var.c_str(), 0, 16));
		}
		return retArray;
	}
	std::vector<DWORD> split_int(const std::string src, const std::string separator){
		std::vector<std::string>  TmpSplit;
		split(src, separator, TmpSplit);
		std::vector<DWORD> retArray;
		for (auto& var : TmpSplit){
			retArray.push_back(StrToIntA((char*)var.c_str()));
		}
		return retArray;
	}


	std::vector<float> split_float(const std::string src, const std::string separator){
		std::vector<std::string>  TmpSplit;
		split(src, separator, TmpSplit);
		std::vector<float> retArray;
		for (auto& var : TmpSplit){
			retArray.push_back(stof(var));
		}
		return retArray;
	}

	std::vector<std::string> split_string(const std::string src, const std::string separator){
		std::vector<std::string>  TmpSplit;
		split(src, separator, TmpSplit);
		return TmpSplit;
	}



	bool str_includeBySplit(const std::string str, const std::string msg, const string separator, bool same){
		std::vector<std::string> tt = split_string(msg, separator);
		for (auto& var : tt){
			if (str_include(str, var, same)){
				return true;
			}
		}

		return false;
	}
	bool str_include(const std::string str, const std::string msg, bool same)//字符串1,字符串2 是否比较完全相同,false=比较是否包含
	{



		if (str.length() == 0 || msg.length() == 0 || str.length() < msg.length())
		{
			return false;
		}
		if (same)
		{
			if (str.length() != msg.length())
			{
				//printf("!SIZE:%d %d",str.size(),msg.size());
				return false;
			}
		}
		auto sour = stringToVecInt(str);
		auto find = stringToVecInt(msg);
		return std::search(sour.begin(), sour.end(), find.begin(), find.end()) != sour.end();
	}

	bool str_include_IC(const std::string str, const std::string msg, bool same){

		/*auto it = std::search(
			strHaystack.begin(), strHaystack.end(),
			strNeedle.begin(), strNeedle.end(),
			[](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
		);
		return (it != strHaystack.end());*/

		if (str.length() == 0 || msg.length() == 0 || str.length() < msg.length()){
			return false;
		}
		if (same){
			if (str.length() != msg.length())
			{
				return false;
			}
		}
		auto sour = stringToVecInt(str);
		auto find = stringToVecInt(msg);
		return std::search(
			sour.begin(), sour.end(), find.begin(), find.end(),
			[](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
		) != sour.end();
	}

	bool StrInVector(std::vector<std::string>vec, std::string str, bool same){
		for (auto& var : vec){
			if (str_include(var, str, same)){
				return true;
			}
		}
		return false;
	}


	bool StrInVector(std::string str, std::vector<std::string>vec, bool same){
		for (auto& var : vec){
			if (str_include(str, var, same)){
				return true;
			}
		}
		return false;
	}

	std::string FillString(std::string  src, int len, std::string ch) {
		int needlen = len - (int)src.size();
		std::string xxx = "";
		int midlen = needlen / 2;
		for (int i = 0; i < needlen; i++){
			if (i == midlen){
				xxx += src;
			}
			xxx += ch;
		}
		return xxx;
	}
	std::string FillString_Left(std::string  src, int len, std::string ch) {
		int needlen = len - (int)src.size();
		std::string xxx = src;
		for (int i = 0; i < needlen; i++){
			xxx += ch;
		}
		return xxx;
	}
};


namespace Str2 {
	/*
	CodeRunTimer::start();
	for (int i = 0; i < 10000000;i++) {
		Str2::isContainAB("撒大苏打实打实大苏打二狗啊啊啊", "二狗啊啊啊");
	}
	CodeRunTimer::end("非kmp");

	CodeRunTimer::start();
	for (int i = 0; i < 10000000; i++) {
		Str2::isContainAB_kmp("撒大苏打实打实大苏打二狗啊啊啊", "二狗啊啊啊");
	}
	CodeRunTimer::end("kmp");

	非kmp:执行耗时 1.034875
	kmp:执行耗时 3.17002
	*/
	bool isContainAB(const std::string& str1, const std::string& str2) {
		return str1.find(str2) != std::string::npos;
	}

	bool isContainABPassCase(const std::string& str1, const std::string& str2) {
		memory::string<memory::new_allocator> str1_lower, str2_lower;
		std::transform(str1.begin(), str1.end(), std::back_inserter(str1_lower), ::tolower);
		std::transform(str2.begin(), str2.end(), std::back_inserter(str2_lower), ::tolower);
		return str1_lower.find(str2_lower) != std::string::npos;
	}

	bool isContainABBA(const std::string& str1, const std::string& str2) {
		return str1.find(str2) != std::string::npos || str2.find(str1) != std::string::npos;
	}
	bool isContainABBAPassCase(const std::string& str1, const std::string& str2) {
		memory::string<memory::new_allocator>  str1_lower, str2_lower;
		std::transform(str1.begin(), str1.end(), std::back_inserter(str1_lower), ::tolower);
		std::transform(str2.begin(), str2.end(), std::back_inserter(str2_lower), ::tolower);

		return str1_lower.find(str2_lower) != std::string::npos || str2_lower.find(str1_lower) != std::string::npos;
	}
	bool isSame(const std::string& str1, const std::string& str2) {
		return str1.compare(str2) == 0;
	}
	bool isSamePassCase(const std::string& str1, const std::string& str2) {
		memory::string<memory::new_allocator>  str1_lower, str2_lower;
		std::transform(str1.begin(), str1.end(), std::back_inserter(str1_lower), ::tolower);
		std::transform(str2.begin(), str2.end(), std::back_inserter(str2_lower), ::tolower);
		return str1_lower.compare(str2_lower) == 0;
	}
	bool isStartWith(const std::string& str, const std::string& prefix) {
		return str.find(prefix) == 0;
	}

	bool isStartWithPassCase(const std::string& str, const std::string& prefix) {
		memory::string<memory::new_allocator>  str1_lower, str2_lower;
		std::transform(str.begin(), str.end(), std::back_inserter(str1_lower), ::tolower);
		std::transform(prefix.begin(), prefix.end(), std::back_inserter(str2_lower), ::tolower);
		return str1_lower.find(str2_lower) == 0;
	}

	std::vector<int> kmp_getNext(const std::string& pattern) {
		int n = pattern.size();
		std::vector<int> next(n, 0);
		for (int i = 1, j = 0; i < n; i++) {
			while (j > 0 && pattern[i] != pattern[j]) {
				j = next[j - 1];
			}
			if (pattern[i] == pattern[j]) {
				j++;
			}
			next[i] = j;
		}
		return next;
	}

	bool isContainAB_kmp(const std::string& text, const std::string& pattern) {
		int n = text.size();
		int m = pattern.size();
		std::vector<int> next = kmp_getNext(pattern);
		for (int i = 0, j = 0; i < n; i++) {
			while (j > 0 && text[i] != pattern[j]) {
				j = next[j - 1];
			}
			if (text[i] == pattern[j]) {
				j++;
			}
			if (j == m) {
				return true/*i - m + 1*/;
			}
		}
		return false;
	}

}

namespace ConsoleConfig {

	bool  isshow = false;
	void trace(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);//设置红色
	}
	void  debug(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE);//设置红色
	}
	void info(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN);//设置红色
	}
	void  warn(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);//设置红色
	}
	void  err(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_INTENSITY);//设置红色
	}
	void critical(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY);//设置红色
	}
	void  colorRed(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED);//设置红色
	}
	void colorWhite(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);//设置三色相加
	}

	void  colorFen(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE);//设置红色和蓝色相加
	}
	void colorQing(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE);//设置绿色和蓝色相加
	}

	void colorGreen(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_GREEN);//设置绿色
	}
	void colorYellow(){
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN);//设置红色和绿色相加
	}
	inline void setsatae(int _showState){
		if (_showState == 1){
			if (isshow){
				return;
			}
			isshow = true;
			AllocConsole();                     // 打开控制台资源
			FILE* f;
			freopen_s(&f, "CONOUT$", "w", stdout);// 申请写
			freopen_s(&f, "CONOUT$", "w", stderr);// 申请写
			freopen_s(&f, "CONIN$", "r", stdin);  // 申请读 
		}
		else{
			if (isshow == false){
				return;
			}
			isshow = false;
			HWND xxx = ::GetConsoleWindow();
			if (xxx != 0)
			{
				::ShowWindow(xxx, SW_HIDE);
			}
			FreeConsole();
		}

	}
}

namespace CodeRunTimer {
	using clock = std::chrono::high_resolution_clock;
	using time_point = std::chrono::time_point<clock>;
	time_point beginTime = {};
	void start() {
		beginTime = clock::now();
	}

	void  end(const char* lable) {
		FPrintf("{}:执行耗时 {}\n", lable, CodeRunTimer::elapsed());
	}
	double elapsed() {
		auto end_time = clock::now();
		auto elapsed_time = std::chrono::duration_cast<std::chrono::microseconds>(end_time - beginTime);
		return elapsed_time.count() / 1000000.0;
	}
}

#include <tuple>
//进程和窗口枚举
namespace WindowProcessTool {


	inline std::vector<PROCESSENTRY32> EmunProcess(){
		vector<PROCESSENTRY32> retArr = {};
		PROCESSENTRY32 PP = {};
		PP.dwSize = sizeof(PROCESSENTRY32);
		HANDLE H_p = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (H_p == INVALID_HANDLE_VALUE)
		{
			return retArr;
			//	 printf("erro");
		}
		BOOL ret = ::Process32First(H_p, &PP);
		while (ret)
		{
			retArr.push_back(PP);
			ret = Process32Next(H_p, &PP);
		}

		::CloseHandle(H_p);
		return retArr;
	}


	inline DWORD  FindProcessByName(const char* Name){
		auto  arr = EmunProcess();
		for (auto& v : arr) {
			if (_strnicmp(Name, v.szExeFile, strlen(Name)) == NULL)
			{
				return v.th32ProcessID;
			}
		}
		return 0;
	}
	inline std::tuple<bool, HWND, DWORD, DWORD>   GetWindowByProcessName(const char* Name)
	{
		auto   pid = FindProcessByName(Name);
		return  GetWindowByProcessID(NULL, pid);
	}

	inline std::string GetProcessNameByProcessID(DWORD  PID) {
		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, PID);
		if (hProcess != NULL) {
			char szProcessName[MAX_PATH] = { 0 };
			if (::GetModuleFileNameExA(hProcess, NULL, szProcessName, MAX_PATH) > 0) {
				CloseHandle(hProcess);
				return std::string(szProcessName);
			}
			CloseHandle(hProcess);
		}
		return std::string("");
	}

	inline std::tuple<bool, HWND, DWORD, DWORD> GetWindowByProcessID(const char* title, DWORD  PID){

		HWND tagHwnd = 0;
		if (PID == 0){
			return std::make_tuple(false, (HWND)0, 0, 0);
		}
		HWND curhw = GetTopWindow(0);
		DWORD  tagThreadid = 0;
		while (curhw != 0){
			DWORD pid;
			tagThreadid = ::GetWindowThreadProcessId(curhw, &pid);
			if (pid == PID)
			{
				if (title != NULL)
				{
					char txt[256] = { 0 };
					::GetWindowTextA(curhw, txt, 256);
					if (strcmp(txt, title) == NULL)
					{
						tagHwnd = curhw;
						break;
					}
				}
				else
				{
					tagHwnd = curhw;
					break;
				}
			}
			curhw = GetNextWindow(curhw, GW_HWNDNEXT);
		}
		if (tagHwnd != 0 && tagThreadid != 0)
		{
			return std::make_tuple(true, tagHwnd, PID, tagThreadid);
		}
		return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
	}


	inline std::tuple<bool, HWND, DWORD, DWORD> GetProcessByTittleAndProcessName(const char* TitleName, const char* ProcessName) {
		HWND tagHwnd = 0;
		HWND curhw = GetTopWindow(0);
		DWORD  tagThreadid = 0;
		DWORD  tagProcessid = 0;
		while (curhw != 0){
			tagThreadid = ::GetWindowThreadProcessId(curhw, &tagProcessid);

			char txt[256] = { 0 };
			::GetWindowTextA(curhw, txt, 256);
			std::string curtxt = txt;
			if (Str::str_include(curtxt, TitleName, false))
			{
				if (FindProcessByName(ProcessName) == tagProcessid)
				{

					tagHwnd = curhw;
					printf("GetClassNameA<%s> hwnd:%x\n", txt, (DWORD)tagHwnd);
					break;
				}
			}
			curhw = GetNextWindow(curhw, GW_HWNDNEXT);
		}
		if (tagHwnd != 0 && tagThreadid != 0 && tagProcessid != 0)
		{
			return std::make_tuple(true, tagHwnd, tagProcessid, tagThreadid);
		}
		return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
	}

	inline std::tuple<bool, HWND, DWORD, DWORD> GetProcessByClassNameAndProcessId(const char* className, DWORD pid){

		HWND tagHwnd = 0;
		HWND curhw = GetTopWindow(0);
		DWORD  tagThreadid = 0;
		DWORD  tagProcessid = 0;
		while (curhw != 0)
		{
			tagThreadid = ::GetWindowThreadProcessId(curhw, &tagProcessid);
			if (tagProcessid == pid)
			{
				char txt[256] = { 0 };
				::GetClassNameA(curhw, txt, 256);
				if (strcmp(txt, className) == NULL)
				{
					tagHwnd = curhw;
					printf("GetClassNameA<%s> hwnd:%x\n", txt, (DWORD)tagHwnd);
					break;
				}
			}
			curhw = GetNextWindow(curhw, GW_HWNDNEXT);
		}
		if (tagHwnd != 0 && tagThreadid != 0 && tagProcessid != 0)
		{
			return std::make_tuple(true, tagHwnd, tagProcessid, tagThreadid);
		}
		return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
	}


	inline std::tuple<bool, HWND, DWORD, DWORD>  GetProcessByClassName(const char* className, bool passCase){
		HWND tagHwnd = 0;
		HWND curhw = GetTopWindow(0);
		DWORD  tagThreadid = 0;
		DWORD  tagProcessid = 0;
		while (curhw != 0)
		{
			tagThreadid = ::GetWindowThreadProcessId(curhw, &tagProcessid);

			char txt[256] = { 0 };
			::GetClassNameA(curhw, txt, 256);
			if ((passCase ? Str2::isContainABPassCase(txt, className) : Str2::isContainAB(txt, className)))
			{
				tagHwnd = curhw;
				printf("GetClassNameA<%s> hwnd:%x\n", txt, (DWORD)tagHwnd);
				break;
			}
			curhw = GetNextWindow(curhw, GW_HWNDNEXT);
		}
		if (tagHwnd != 0 && tagThreadid != 0 && tagProcessid != 0)
		{
			return std::make_tuple(true, tagHwnd, tagProcessid, tagThreadid);
		}
		return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
	}
	inline std::tuple<bool, HWND, DWORD, DWORD> GetProcessByTittleName(const char* TitleName){
		HWND tagHwnd = 0;
		HWND curhw = GetTopWindow(0);
		DWORD  tagThreadid = 0;
		DWORD  tagProcessid = 0;
		while (curhw != 0)
		{
			tagThreadid = ::GetWindowThreadProcessId(curhw, &tagProcessid);

			char txt[256] = { 0 };
			::GetWindowTextA(curhw, txt, 256);
			std::string curtxt = txt;
			if (Str::str_include(curtxt, TitleName, false))
			{
				tagHwnd = curhw;
				printf("GetClassNameA<%s> hwnd:%x\n", txt, (DWORD)tagHwnd);
				break;
			}
			curhw = GetNextWindow(curhw, GW_HWNDNEXT);
		}
		if (tagHwnd != 0 && tagThreadid != 0 && tagProcessid != 0)
		{
			return std::make_tuple(true, tagHwnd, tagProcessid, tagThreadid);
		}
		return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
	}
	//提权
	inline bool  EnableDebugPrivilege(){
		HANDLE hToken;
		LUID sedebugnameValue;
		TOKEN_PRIVILEGES tkp = {};
		if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		{
			return   FALSE;
		}
		if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &sedebugnameValue))
		{
			CloseHandle(hToken);
			return false;
		}
		tkp.PrivilegeCount = 1;
		tkp.Privileges[0].Luid = sedebugnameValue;
		tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		if (!AdjustTokenPrivileges(hToken, FALSE, &tkp, sizeof(tkp), NULL, NULL))
		{
			CloseHandle(hToken);
			return false;
		}
		return true;
	}


	inline std::tuple<bool, HWND, DWORD, DWORD>  GetProcessByTittleAndClassName(const char* TitleName, const char* Classname){
		if (!TitleName && !Classname)
		{

			return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
		}
		HWND tagHwnd = 0;
		HWND curhw = GetTopWindow(0);
		DWORD  tagThreadid = 0;
		DWORD  tagProcessid = 0;
		while (curhw != 0)
		{
			tagThreadid = ::GetWindowThreadProcessId(curhw, &tagProcessid);
			bool titlieOk = false;
			bool classOk = false;
			if (TitleName != NULL)
			{
				char txt[256] = { 0 };
				::GetWindowTextA(curhw, txt, 256);
				std::string curtxt = txt;
				printf("Title CMP:%s,%s\n", curtxt.c_str(), TitleName);
				if (Str::str_include(curtxt, TitleName, false))
				{
					titlieOk = true;
				}
			}
			else
			{
				titlieOk = true;
			}

			if (Classname != NULL)
			{
				char txt[256] = { 0 };
				::GetClassNameA(curhw, txt, 256);
				std::string curtxt = txt;
				printf("\tCLass CMP:%s,%s\n", curtxt.c_str(), Classname);
				if (Str::str_include_IC(curtxt, Classname, true))//不区分大小写
				{
					classOk = true;
				}
			}
			else
			{
				classOk = true;
			}
			if (titlieOk && classOk)
			{
				tagThreadid = ::GetWindowThreadProcessId(curhw, &tagProcessid);
				tagHwnd = curhw;
				break;
			}
			curhw = GetNextWindow(curhw, GW_HWNDNEXT);
		}
		if (tagHwnd != 0 && tagThreadid != 0 && tagProcessid != 0)
		{
			return std::make_tuple(true, tagHwnd, tagProcessid, tagThreadid);
		}
		return std::make_tuple<bool, HWND, DWORD>(false, 0, 0, 0);
	}

}



namespace MyFileApi {
	memory::vector<BYTE, memory::new_allocator> readBigFile(std::string fileName) {
		memory::vector<BYTE, memory::new_allocator> fileData = {};
		HANDLE hFile = ::CreateFileA(fileName.c_str(),
			GENERIC_READ,
			FILE_SHARE_READ,
			0,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0);
		if (hFile == INVALID_HANDLE_VALUE) {
			MySdk::ErrorOut("ReadBigFile 失败:%s", fileName.c_str());
		}
		LARGE_INTEGER large_integer = { 0 };
		if (!::GetFileSizeEx(hFile, &large_integer)) {
			AfxMessageBox("ReadBigFile 获取大小失败");
		}
		size_t file_size = (size_t)large_integer.QuadPart;
		HANDLE map_file = ::CreateFileMappingA(hFile,
			0,
			PAGE_READONLY,
			0,
			0,
			nullptr);
		if (map_file == INVALID_HANDLE_VALUE || map_file == NULL) {
			AfxMessageBox("ReadBigFile 文件映射失败");
			return fileData;
		}
		LPVOID pMap = ::MapViewOfFile(map_file, FILE_MAP_READ, 0, 0, 0);
		if (pMap == nullptr) {
			AfxMessageBox("ReadBigFile 文件映射 MapViewOfFile失败");
			return fileData;
		}
		fileData.resize(file_size);
		if (fileData.size() == file_size) {
			RtlCopyMemory(fileData.data(), pMap, file_size);
		}
		else {
			AfxMessageBox("ReadBigFile 文件映射失败 malloc");
		}
		::UnmapViewOfFile(pMap);
		::CloseHandle(map_file);
		::CloseHandle(hFile);
		return fileData;
	}

	memory::string<memory::new_allocator> readBigStringFile(std::string fileName) {
		auto  xx = readBigFile(fileName);
		return MySdk::memoryVectorToString(xx);
	}
	std::stringstream readFileByStl(std::string FilePath){

		std::ifstream f;
		f.open(FilePath, std::ios::in | std::ios::binary);
		std::stringstream tmp;
		tmp << f.rdbuf();
		return tmp;
	}

	std::string readFileStringByStl(std::string FilePath){
		std::ifstream f;
		f.open(FilePath, std::ios::in);
		std::stringstream tmp;
		tmp << f.rdbuf();
		return tmp.str();
	}

	int getFileSize(std::string FilePath){

		std::ifstream f;
		f.open(FilePath, std::ios::in | std::ios::binary);
		if (f.is_open() == false)
		{
			return 0;
		}
		if (!f)
		{
			return 0;
		}
		f.seekg(0, ios_base::end);
		int nFileLen = (int)f.tellg();
		f.close();
		return nFileLen;
	}
	int getFileSizeByApi(std::string FilePath){
		auto [ok, fHandle] = openFileByApi(FilePath);
		if (ok) {
			int sz = (int)::GetFileSize(fHandle, 0);
			::CloseHandle(fHandle);
			return sz;
		}
		return 0;
	}
	int getFileSizeByApi(HANDLE fileHandle){
		return  (int)::GetFileSize(fileHandle, 0);
	}
	std::tuple<bool, HANDLE> openFileByApi(std::string FilePath, bool canWrite) {
		DWORD gMode = GENERIC_READ;
		if (canWrite) {
			gMode |= GENERIC_WRITE;
		}
		HANDLE m_fasetWriteHandle = ::CreateFileA(FilePath.c_str(), gMode, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
		if (m_fasetWriteHandle == INVALID_HANDLE_VALUE || m_fasetWriteHandle == 0)
		{
			MySdk::ErrorOutExit(Str::string_sprintf("打开文件失败 OpenFileByApi ExirCode:%d", ::GetLastError()).c_str(), 1000);
			return std::make_tuple(false, INVALID_HANDLE_VALUE);
		}
		return std::make_tuple(true, m_fasetWriteHandle);
	}
	memory::vector<UCHAR, memory::new_allocator> readFileByApi(std::string FilePath){
		memory::vector<UCHAR, memory::new_allocator> x;
		auto [ok, fHandle] = openFileByApi(FilePath);
		if (ok) {
			int fsize = getFileSizeByApi(fHandle);
			if (fsize > 0) {
				x.resize(fsize);
				DWORD numberBread = 0;
				if (::ReadFile(fHandle, x.data(), fsize, &numberBread, 0) == FALSE) {
					MySdk::ErrorOutExit(Str::string_sprintf("ReadFileByApi->ReadFile  ExirCode:%d", ::GetLastError()).c_str(), 1000);
				}
			}
		}
		::CloseHandle(fHandle);
		return x;
	}


	memory::string<memory::new_allocator> readFileStringByApi(std::string FilePath) {
		auto  dt = readFileByApi(FilePath);
		return MySdk::memoryVectorToString(dt);
	}

	///////////////////////////////写///////////////////////////////////////////
	/*
		ios_base::in	以读取方式打开文件。
ios_base::out	以写入方式打开文件。
ios_base::binary	以二进制模式打开文件。
ios_base:: ate	打开文件的时候，定位到文件的末尾。
ios_base:: app	以追加方式打开文件，所有写文件的数据都是追加在文件末尾。
ios_base::trunc	打开文件时，但是文件之前的内容都会被清空。

	std::string ReadFile(const std::string& filepath) {
		std::ifstream ifs(filepath.c_str());
		std::string content((std::istreambuf_iterator<char>(ifs)),
			(std::istreambuf_iterator<char>()));
		ifs.close();
		return content;
	}

	void WriteFile(const std::string& filepath, const std::string& content) {
		std::ofstream ofs(filepath.c_str(), std::ios_base::binary | ios::trunc);//必须BIN模式 不然一些文本的 \r\n会变成\r\r\n
		ofs << content;
		ofs.close();
		return;
	}
	*/
	bool  writeFileStl(string file, BYTE* data, int len, bool appendMode){
		auto mode = std::ios::out | std::ios::binary;
		if (!appendMode) {
			mode = mode | std::ios::trunc;//存在则删除
		}
		else
		{
			mode = mode | std::ios::app;
		}
		std::ofstream f;
		f.open(file, mode);
		if (f.fail())
		{
			return false;
		}
		f.write((const char*)data, len);
		f.close();
		return true;
	}
	bool  writeFileStringStl(string file, std::string s, bool appendMode){
		int mode = std::ios::out;
		if (!appendMode) {
			mode = mode | std::ios::trunc;//存在则删除
		}
		else
		{
			mode = mode | std::ios::app;
		}
		std::ofstream f;
		f.open(file, mode);
		if (f.fail())
		{
			return false;
		}
		f << s;
		f.close();
		return true;
	}
	//API写
	bool writeFileApi(string file, BYTE* data, int len,bool appendMode){
		HANDLE m_fasetWriteHandle = INVALID_HANDLE_VALUE;
		DWORD md = 0;
		if (appendMode) {
			md = OPEN_ALWAYS;
		}else
		{
			md = CREATE_ALWAYS;
		}
		while (true)
		{
			m_fasetWriteHandle = ::CreateFileA(file.c_str(), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, md, FILE_ATTRIBUTE_NORMAL, 0);
			if (m_fasetWriteHandle == INVALID_HANDLE_VALUE || m_fasetWriteHandle == 0)
			{
				MySdk::ErrorOutExit(Str::string_sprintf("WriteFileApi,Error:%d", ::GetLastError()).c_str(), 1000);
				return false;
			}
			else
			{
				break;
			}
		}
		if (appendMode) {
			::SetFilePointer(m_fasetWriteHandle, 0, NULL, FILE_END);
		}
		DWORD bread;
		::WriteFile(m_fasetWriteHandle, data, len, &bread, NULL);
		::FlushFileBuffers(m_fasetWriteHandle);
		::CloseHandle(m_fasetWriteHandle);
		::WaitForSingleObject(m_fasetWriteHandle, INFINITE);
		return true;
	}
	bool writeFileStringApi(string file,string s, bool appendMode){
		return writeFileApi(file, (BYTE*)s.c_str(), s.size(), appendMode);
	}
} // namespace MyFileReader



namespace PathApi {
	std::string  g_App_curExePath="";
	std::string  g_App_parentPath = "";
	std::string  g_App_curExeName = "";
	void recordAppPath(){
		g_App_curExePath = getCurExePathName();
		filesystem::path P = g_App_curExePath;
		g_App_parentPath = P.parent_path().generic_string();
		g_App_curExeName = P.filename().generic_string();
	}

	//当前全路径包含EXE  Q:\NewClass\Release\NewClass.exe
	std::string getRecordAppExeFullPath() {
		if (g_App_curExePath == "")
		{
			MySdk::ErrorOutExit("getRecordAppExeFullPath Error");
		}
		return  g_App_curExePath;
	}

	std::string getRecordAppExeName() {
		if (g_App_curExeName == "")
		{
			MySdk::ErrorOutExit("getRecordAppExeName Error");
		}
		return  g_App_curExeName;
	}
	//当前全路径 仅文件夹 Q:\NewClass\Release
	std::string getRecordAppDir() {
		if (g_App_parentPath == "")
		{
			MySdk::ErrorOutExit("getRecordAppDir Error");
		}
		return  g_App_parentPath;
	}
	void fixAppCurPath(){
		setCurRentPath(getRecordAppDir());
	}
	void setCurRentPath(string ph){
		//::SetCurrentDirectoryA(ph.c_str());
		filesystem::current_path(ph);
	}
	inline string getCurExePathName(){

		char moduleFileName[MAX_PATH];
		::GetModuleFileNameA(GetModuleHandleA(NULL), moduleFileName, MAX_PATH);
		return string(moduleFileName);
	}
	inline string getCurExePath(){
		auto  exepathName = getCurExePathName();
		return getDirByFullPath(exepathName);
	}

	inline string getDirByFullPath(string phName) {
		std::string strPath(phName);
		std::string::size_type pos = strPath.find_last_of("\\/");
		if (pos != std::string::npos) {
			strPath = strPath.substr(0, pos);
		}
		return strPath;
	}
	inline string getDirByFullPath2(string phName) {
		filesystem::path P = phName;
		return P.parent_path().generic_string();
	}
	inline string getFileNameByFullPath(string phName) {
		filesystem::path P = phName;
		return P.filename().generic_string();
	}
	inline string getFileExtByFullPath(string phName) {
		filesystem::path P = phName;
		return P.extension().generic_string();
	}

	vector<string> getDirFiles(string dir,string  ext) {
		auto  xxx = filesystem::directory_iterator(dir);//directory_iterator是单目录 ,recursive_directory_iterator是递归 是包含子目录
		string all = "";
		vector<string> ret;
		for (auto& v : xxx)
		{
			if (v.is_directory())
			{
				//FPrintf("递归遍历目录:{}\n", v.path().generic_string());
			}
			else
			{
				if (ext == "*" ||( v.path().has_extension() && v.path().extension().generic_string() == ext)) {
					ret.push_back(v.path().generic_string());
				}
			}
		}
		return ret;
	}
	vector<string> getDirFilesEx(string dir, string  ext) {
		auto  xxx = filesystem::recursive_directory_iterator(dir);//递归
		string all = "";
		vector<string> ret;
		for (auto& v : xxx)
		{
			if (v.is_directory())
			{
				//FPrintf("递归遍历目录:{}\n", v.path().generic_string());
			}
			else
			{
				if (ext == "*" || (v.path().has_extension() && v.path().extension().generic_string() == ext)) {
					ret.push_back(v.path().generic_string());
				}
			}
		}
		return ret;
	}

	string relativePathToAbsPath(string pt) {
		return filesystem::absolute(pt).generic_string();
	}
	string absPathToRelativePath(string pt) {
		//这里有个参数 是假设的当前目录 不传入默认是当前目录
		return filesystem::relative(pt).generic_string();
	}


	bool Exist(string fileOrPath) {
		return filesystem::exists(fileOrPath);
	}

	bool CopyEx(string f, string t) {
		/*
			否则，若 copy_options::skip_existing 设置于 options ，则不做任何事
		否则，若 copy_options::overwrite_existing 设置于 options ，则复制 from 所解析到的文件的内容及属性到 to 所解析到的文件
		否则，若 copy_options::update_existing 设置于 options ，则仅若 from 按 last_write_time() 定义新于 to 才复制
		*/
		try
		{
			//覆盖模式
			filesystem::copy(f, t, filesystem::copy_options::overwrite_existing);
		}
		catch (std::filesystem::filesystem_error& e)
		{
			MySdk::ErrorOutExit("CopyEx filesystem_error:%s", e.what());
		}
	}
	bool Delete(string f) {
		try
		{
			filesystem::remove_all(f);
		}
		catch (std::filesystem::filesystem_error& e)
		{
			MySdk::ErrorOutExit("Delete filesystem_error:%s", e.what());
		}
	}

	void  Create(string pt) {
		try
		{
			filesystem::create_directories(pt);
		}
		catch (std::filesystem::filesystem_error& e)
		{
			MySdk::ErrorOutExit("Create filesystem_error:%s", e.what());
		}
	}
}


namespace stringConv {
	inline std::string Win32_Utf8ToAnsi(const std::string utf8){
		std::string str = "";
		int len = MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), -1, NULL, 0);
		if (len <= 0)
			return str;
		wchar_t* wstr = new wchar_t[len + 1];
		memset(wstr, 0, len + 1);
		MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), -1, wstr, len);
		len = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);
		if (len <= 0)
			return str;
		char* buf = new char[len + 1];
		memset(buf, 0, len + 1);
		WideCharToMultiByte(CP_ACP, 0, wstr, -1, buf, len, NULL, NULL);
		str.assign(buf, (int)len - 1);
		delete[] wstr;
		delete[] buf;
		return std::move(str);
	}

	std::string Win32_Utf8ToAnsi_2(const std::string utf8str)
	{
		int requiredSize = MultiByteToWideChar(CP_UTF8, 0, utf8str.c_str(), -1, NULL, 0);
		if (requiredSize == 0) {
			return "";
		}

		std::wstring utf16str(requiredSize, 0);
		int convertedSize = MultiByteToWideChar(CP_UTF8, 0, utf8str.c_str(), -1, &utf16str[0], requiredSize);
		if (convertedSize == 0) {
			return "";
		}

		requiredSize = WideCharToMultiByte(CP_ACP, 0, utf16str.c_str(), -1, NULL, 0, NULL, NULL);
		if (requiredSize == 0) {
			return "";
		}

		std::string ansi(requiredSize, 0);
		convertedSize = WideCharToMultiByte(CP_ACP, 0, utf16str.c_str(), -1, &ansi[0], requiredSize, NULL, NULL);
		if (convertedSize == 0) {
			return "";
		}

		return ansi;
	}
	inline std::string Win32_Utf8ToAnsi(const char* utf8){
		std::string str;
		int len = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, 0);
		if (len <= 0)
			return str;
		wchar_t* wstr = new wchar_t[len + 1];
		memset(wstr, 0, len + 1);
		MultiByteToWideChar(CP_UTF8, 0, utf8, -1, wstr, len);
		len = WideCharToMultiByte(CP_ACP, 0, wstr, -1, NULL, 0, NULL, NULL);
		char* buf = new char[len + 1];
		memset(buf, 0, len + 1);
		WideCharToMultiByte(CP_ACP, 0, wstr, -1, buf, len, NULL, NULL);

		str.assign(buf, (int)len - 1);
		delete[] wstr;
		delete[] buf;
		return std::move(str);
	}

	inline std::string Win32_AnsiToUtf8(const char* gb2312){
		std::string str;
		int len = MultiByteToWideChar(CP_ACP, 0, gb2312, -1, NULL, 0);
		if (len <= 0)
			return str;
		wchar_t* wstr = new wchar_t[len + 1];
		memset(wstr, 0, len + 1);
		MultiByteToWideChar(CP_ACP, 0, gb2312, -1, wstr, len);
		len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
		char* buf = new char[len + 1];
		memset(buf, 0, len + 1);
		WideCharToMultiByte(CP_UTF8, 0, wstr, -1, buf, len, NULL, NULL);

		str.assign(buf, (int)(len - 1));
		delete[] wstr;
		delete[] buf;
		return std::move(str);
	}

	inline std::string Win32_AnsiToUtf8(const std::string gb2312){
		std::string str="";
		int len = MultiByteToWideChar(CP_ACP, 0, gb2312.c_str(), -1, NULL, 0);
		if (len == 0) {
			return "";
		}
		wchar_t* wstr = new wchar_t[len + 1];
		memset(wstr, 0, len + 1);
		MultiByteToWideChar(CP_ACP, 0, gb2312.c_str(), -1, wstr, len);
		len = WideCharToMultiByte(CP_UTF8, 0, wstr, -1, NULL, 0, NULL, NULL);
		if (len == 0) {
			return "";
		}
		char* buf = new char[len + 1];
		memset(buf, 0, len + 1);
		WideCharToMultiByte(CP_UTF8, 0, wstr, -1, buf, len, NULL, NULL);

		str.assign(buf, (int)(len - 1));
		delete[] wstr;
		delete[] buf;
		return std::move(str);
	}
	//该函数先将 ANSI 转为 UTF-16，然后再将 UTF-16 转为 UTF-8。如果转换过程中出现问题，则返回空字符串。
	std::string Win32_AnsiToUtf8_2(const std::string ansi)
	{
		int len = MultiByteToWideChar(CP_ACP, 0, ansi.c_str(), -1, nullptr, 0);
		if (len == 0) {
			return "";
		}
		std::wstring utf16(len, L'\0');
		len = MultiByteToWideChar(CP_ACP, 0, ansi.c_str(), -1, &utf16[0], len);
		if (len == 0) {
			return "";
		}
		len = WideCharToMultiByte(CP_UTF8, 0, utf16.c_str(), -1, nullptr, 0, nullptr, nullptr);
		if (len == 0) {
			return "";
		}
		std::string utf8(len, '\0');
		len = WideCharToMultiByte(CP_UTF8, 0, utf16.c_str(), -1, &utf8[0], len, nullptr, nullptr);
		if (len == 0) {
			return "";
		}
		return utf8;
	}
	inline std::wstring Win32_StringToWstring(std::string& strascii){
		int widesize = MultiByteToWideChar(CP_ACP, 0, (char*)strascii.c_str(), -1, NULL, 0);
		if (widesize == ERROR_NO_UNICODE_TRANSLATION)
		{
			MySdk::ErrorOutExit("Win32_StringToWstring Error 0");
		}
		if (widesize == 0)
		{
			MySdk::ErrorOutExit("Win32_StringToWstring Error 1");
		}
		std::vector<wchar_t> resultstring(widesize);
		int convresult = MultiByteToWideChar(CP_ACP, 0, (char*)strascii.c_str(), -1, &resultstring[0], widesize);
		if (convresult != widesize)
		{
			MySdk::ErrorOutExit("Win32_StringToWstring Error 2");
		}

		return std::wstring(&resultstring[0]);
	}

	inline std::string Win32_WstringToString(const std::wstring& widestring){
		int utf8size = ::WideCharToMultiByte(CP_UTF8, 0, widestring.c_str(), -1, NULL, 0, NULL, NULL);
		if (utf8size == 0)
		{
			MySdk::ErrorOutExit("Error in conversion.");
		}
		std::vector<char> resultstring(utf8size);

		int convresult = ::WideCharToMultiByte(CP_UTF8, 0, widestring.c_str(), -1, &resultstring[0], utf8size, NULL, NULL);

		if (convresult != utf8size)
		{
			MySdk::ErrorOutExit("Win32_WstringToString La falla!");
		}

		return std::string(&resultstring[0]);
	}
}

/////////////////////////////////////内存加载/////////////////////////////////////
bool MemoryLoadClass::LoadDll(const void* buffer)
{
	m_Moudle = MemoryLoadLibrary(buffer);
	return m_Moudle == NULL ? false : true;
}

DWORD MemoryLoadClass::MemoryGetProcAddress(const char* fn)
{
	if (!m_Moudle)
	{
		return 0;
	}
	return (DWORD)::MemoryGetProcAddress(m_Moudle, fn);
}
void MemoryLoadClass::MemoryFreeLibrary()
{
	if (m_Moudle)
	{
		::MemoryFreeLibrary(m_Moudle);
		m_Moudle = NULL;
	}
}

////////////////////////////////汇编引擎内存加载//////////////////////////////////////////

namespace AsmkeyStoneDll {

	MemoryLoadClass memload;
	_ks_version ks_version = nullptr;
	_ks_arch_supported ks_arch_supported = nullptr;
	_ks_open ks_open = nullptr;
	_ks_close ks_close = nullptr;
	_ks_errno ks_errno = nullptr;
	_ks_strerror ks_strerror = nullptr;
	_ks_option ks_option = nullptr;
	_ks_asm ks_asm = nullptr;
	_ks_free ks_free = nullptr;
	bool beInit = false;
	std::string AsmkeyStoneDll_path = "null";
	void Init(std::string pt) {
		AsmkeyStoneDll_path = pt;
		if (!beInit) {
			if (AsmkeyStoneDll_path == "null") {
				FatalAppExit(-1, "AsmkeyStoneDll::Init() AsmkeyStoneDll_path=Null");
			}
			std::stringstream tmp = MyFileApi::readFileByStl(pt);
			InitFromMemoty(tmp.str().data());
		}
	}
	void InitFromMemoty(const void* buf) {
		memload.LoadDll(buf);
		ks_version =
			(decltype(ks_version))memload.MemoryGetProcAddress("ks_version");
		ks_arch_supported =
			(decltype(ks_arch_supported))memload.MemoryGetProcAddress(
				"ks_arch_supported");
		ks_open = (decltype(ks_open))memload.MemoryGetProcAddress("ks_open");
		ks_close = (decltype(ks_close))memload.MemoryGetProcAddress("ks_close");
		ks_errno = (decltype(ks_errno))memload.MemoryGetProcAddress("ks_errno");
		ks_strerror =
			(decltype(ks_strerror))memload.MemoryGetProcAddress("ks_strerror");
		ks_option = (decltype(ks_option))memload.MemoryGetProcAddress("ks_option");
		ks_asm = (decltype(ks_asm))memload.MemoryGetProcAddress("ks_asm");
		ks_free = (decltype(ks_free))memload.MemoryGetProcAddress("ks_free");
		if (!ks_option || !ks_arch_supported || !ks_asm || !ks_open) {
			FatalAppExit(-1, "MemoryKeyStoneAPI Init Error\n");
		}
		// printf("addressTest：%x\n", ks_asm);
		beInit = true;
	}
	std::vector<BYTE> Assemble32(std::string p_sASM, DWORD adresss) {

		if (!beInit)
		{
			MySdk::ErrorOutExit("AsmkeyStoneDll Not Init");
		}
		std::vector<BYTE> RET;
		ks_engine* ks;
		ks_err err = KS_ERR_ARCH;
		size_t count;
		//	unsigned char* encode;
		size_t size;

		// Open keystone engine

		err = ks_open(KS_ARCH_X86, KS_MODE_32, &ks);

		if (err != KS_ERR_OK) {
			std::cout << "ERROR: Failed on ks_open()" << endl;
			return RET;
		}

		// Assemble the code

		ks_option(ks, KS_OPT_SYNTAX, KS_OPT_SYNTAX_MASM/*KS_OPT_SYNTAX_INTEL*//*KS_OPT_SYNTAX_NASM*/);
		unsigned char* encode;
		if (ks_asm(ks, p_sASM.c_str(), adresss, &encode, &size, &count)) {
			std::cout << "ERROR: Failed on ks_asm() with count = " << count << ", error code = " << ks_errno(ks) << " " << ks_strerror(ks_errno(ks)) << endl;
			return RET;
		}

		// If everything was OK, set the size and output and return

		RET.resize(size);

		memcpy(RET.data(), encode, size);

		// Assembly information 

		std::cout << "Assembled: " << size << " bytes, " << count << " statements" << endl;

		// Cleanup allocated data

		ks_free(encode);
		ks_close(ks);

		return RET;
	}
	std::vector<BYTE> Assemble64(std::string p_sASM, DWORD64 adresss) {

		if (!beInit)
		{
			MySdk::ErrorOutExit("AsmkeyStoneDll Not Init");
		}
		std::vector<BYTE> RET;
		ks_engine* ks = 0;
		ks_err err = KS_ERR_ARCH;
		size_t count = 0;
		//	unsigned char* encode;
		size_t size = 0;

		// Open keystone engine

		err = ks_open(KS_ARCH_X86, KS_MODE_64, &ks);

		if (err != KS_ERR_OK) {
			std::cout << "ERROR: Failed on ks_open()" << endl;
			return RET;
		}

		// Assemble the code

		ks_option(ks, KS_OPT_SYNTAX, KS_OPT_SYNTAX_INTEL/*KS_OPT_SYNTAX_NASM*/);
		unsigned char* encode;
		if (ks_asm(ks, p_sASM.c_str(), adresss, &encode, &size, &count)) {
			std::cout << "encode sz:" << size << std::endl;
			std::cout << "ERROR: Failed on ks_asm() with count = " << count << ", error code = " << ks_errno(ks) << " " << ks_strerror(ks_errno(ks)) << endl;
			return RET;
		}

		// If everything was OK, set the size and output and return

		RET.resize(size);

		memcpy(RET.data(), encode, size);

		// Assembly information 
		//调试 输出
		//std::cout << "Assembled: " << size << " bytes, " << count << " statements" << endl;

		// Cleanup allocated data

		ks_free(encode);
		ks_close(ks);

		return RET;
	}
};



namespace AsmCapStone {
	std::string  Capsttone_diasm64(DWORD64 code, int size, DWORD64 baseadresss, int strSize){
		//解析汇编我只给了1000字节
		std::string ret = "";
		ret.reserve(strSize);
		csh caphandle;
		cs_err err = cs_open(CS_ARCH_X86, CS_MODE_64, &caphandle);
		if (err) {
			MySdk::ErrorOutExit("Failed on cs_open() with error returned: %u %s\n", err, cs_strerror(err));
		}
		cs_option(caphandle, CS_OPT_DETAIL, CS_OPT_ON);
		cs_insn* insn;
		int count = cs_disasm(caphandle, (uint8_t*)code, size, baseadresss, 0, &insn);
		if (count) {
			size_t j;
			int curpos = 0;
			for (j = 0; j < count; j++) {
				ret += Str::string_sprintf("%d 0x%llx: \t %s\t %s\r\n", j + 1, insn[j].address, insn[j].mnemonic, insn[j].op_str);
				FPrintf("{}\r\n",insn[j].op_str);
				curpos += insn[j].size;
			}
			cs_free(insn, count);
		}
		cs_close(&caphandle);
		return ret;
	}


	std::string  Capsttone_diasm64_real(DWORD64 code, int size, DWORD64 baseadresss, int strSize) {
		//解析汇编我只给了1000字节
		std::string ret = "";
		ret.reserve(strSize);
		csh caphandle;
		cs_err err = cs_open(CS_ARCH_X86, CS_MODE_64, &caphandle);
		if (err) {
			MySdk::ErrorOutExit("Failed on cs_open() with error returned: %u %s\n", err, cs_strerror(err));
		}
		cs_option(caphandle, CS_OPT_DETAIL, CS_OPT_ON);
		cs_insn* insn;
		int count = cs_disasm(caphandle, (uint8_t*)code, size, baseadresss, 0, &insn);
		if (count) {
			size_t j;
			int curpos = 0;
			for (j = 0; j < count; j++) {
				ret += Str::string_sprintf("%s \t  %s\r\n",insn[j].mnemonic, insn[j].op_str);
				curpos += insn[j].size;
			}
			cs_free(insn, count);
		}
		cs_close(&caphandle);
		return ret;
	}

	std::string  Capsttone_diasm32_real(DWORD64 code, int size, DWORD64 baseadresss, int strSize) {
		//解析汇编我只给了1000字节
		std::string ret = "";
		ret.reserve(strSize);
		csh caphandle;
		cs_err err = cs_open(CS_ARCH_X86, CS_MODE_32, &caphandle);
		if (err) {
			MySdk::ErrorOutExit("Failed on cs_open() with error returned: %u %s\n", err, cs_strerror(err));
		}
		cs_option(caphandle, CS_OPT_DETAIL, CS_OPT_ON);
		cs_insn* insn;
		int count = cs_disasm(caphandle, (uint8_t*)code, size, baseadresss, 0, &insn);
		if (count) {
			size_t j;
			int curpos = 0;
			for (j = 0; j < count; j++) {
				ret += Str::string_sprintf("%s \t  %s\r\n", insn[j].mnemonic, insn[j].op_str);
				curpos += insn[j].size;
			}
			cs_free(insn, count);
		}
		cs_close(&caphandle);
		return ret;
	}
	std::tuple<bool, DWORD64, DWORD64> Capsttone_GetAsmDispAddress64(DWORD64 codeAddress, int size, DWORD64 baseadresss, int asmLine)
	{

		csh caphandle;
		cs_err err = cs_open(CS_ARCH_X86, CS_MODE_64, &caphandle);
		if (err) {
			MySdk::ErrorOutExit("Failed on cs_open() with error returned: %u\n", err);
		}
		cs_option(caphandle, CS_OPT_DETAIL, CS_OPT_ON);

		cs_insn* insn;
		int count = cs_disasm(caphandle, (uint8_t*)codeAddress, size, baseadresss, 0, &insn);
		if (count) {
			cs_insn cur = insn[asmLine - 1];
			//printf("   调试信息-> 地址:%llx: %s  %s \n", cur.address, cur.mnemonic, cur.op_str);
			if (insn[asmLine - 1].detail->x86.encoding.disp_offset != 0)
			{

				cs_free(insn, count);
				return std::make_tuple(true, cur.address, insn[asmLine - 1].detail->x86.disp);
			}
			cs_free(insn, count);
			return std::make_tuple(false, cur.address,-1);
		}
		else
		{
			printf("count=:%d\n", count);
		}

		cs_free(insn, count);
		return std::make_tuple(false, -1, -1);
	}



	std::tuple<bool, DWORD64, DWORD64> Capsttone_GetAsmDispAddress64_FixRip(DWORD64 codeAddress, int size, DWORD64 baseadresss, int asmLine)
	{

		csh caphandle;
		cs_err err = cs_open(CS_ARCH_X86, CS_MODE_64, &caphandle);
		if (err) {
			MySdk::ErrorOutExit("Failed on cs_open() with error returned: %u\n", err);
		}
		cs_option(caphandle, CS_OPT_DETAIL, CS_OPT_ON);

		cs_insn* insn;
		int count = cs_disasm(caphandle, (uint8_t*)codeAddress, size, baseadresss, 0, &insn);
		if (count) {
			cs_insn cur = insn[asmLine - 1];
			//printf("   调试信息-> 地址:%llx: %s  %s \n", cur.address, cur.mnemonic, cur.op_str);
			if (insn[asmLine - 1].detail->x86.encoding.disp_offset != 0)
			{
				DWORD64 code  = (
					(uint64_t)cur.address +
					cur.detail->x86.disp +
					cur.detail->x86.encoding.disp_size +
					cur.detail->x86.encoding.disp_offset
					);

				cs_free(insn, count);


				//算法对不对需要研究 实战中我是发现能将就用
				return std::make_tuple(true, cur.address, code);
			}
			cs_free(insn, count); 
			return std::make_tuple(false, cur.address, -1);
		}

		cs_free(insn, count);
		return std::make_tuple(false, -1, -1);
	}



	std::tuple<bool, DWORD64, DWORD64>  Capsttone_GetAsmImmAddress64(DWORD64 codeAddress, int size, DWORD64 baseadresss, int asmLine)
	{


		csh caphandle;
		cs_err err = cs_open(CS_ARCH_X86, CS_MODE_64, &caphandle);
		if (err) {
			MySdk::ErrorOutExit("Failed on cs_open() with error returned: %u\n", err);
		}
		cs_option(caphandle, CS_OPT_DETAIL, CS_OPT_ON);
		cs_insn* insn;
		int count = cs_disasm(caphandle, (uint8_t*)codeAddress, size, baseadresss, 0, &insn);
		if (count) {
			DWORD64  addressCall = 0;
			DWORD64 adr=0;
			cs_insn cur = insn[asmLine - 1];
			for (int i = 0; i < 8; i++)
			{
				if (cur.detail->x86.operands[i].type == x86_op_type::X86_OP_IMM)
				{
					//	printf("ASMLINE:%d 0x%llx %s  %s \n", asmLine,cur.address, cur.mnemonic, cur.op_str);
					adr = cur.address;
					addressCall = (DWORD64)cur.detail->x86.operands[i].imm;
					break;
				}
			}
			/*if (cur.detail->x86.operands[0].type == x86_op_type::X86_OP_IMM)
			{
				addressCall = (DWORD)cur.detail->x86.operands[0].imm;
			}
			else if (cur.detail->x86.operands[0].type == x86_op_type::X86_OP_REG)
			{
				addressCall = (DWORD)cur.detail->x86.operands[0].mem.disp;
			}*/
			//printf("   调试信息-> 地址:%llx: %s  %s \n",  cur.address, cur.mnemonic, cur.op_str);
			cs_free(insn, count);
			return make_tuple(true,adr,addressCall);
		}
		else
		{
			printf("ERRO COUNT=0\n");
		}

		cs_free(insn, count);
		return make_tuple(false, -1,-1);
	}

}

namespace AsmExt {
	DWORD64  乾坤大挪移(DWORD64 src, int maybeSize) {
		DWORD dwOldProtect;
		::VirtualProtect((LPVOID)src, maybeSize, PAGE_EXECUTE_READWRITE, &dwOldProtect);
		auto _asmcode = AsmCapStone::Capsttone_diasm32_real(src, maybeSize, src, maybeSize);
		char* buf = new char[maybeSize];
		memset(buf, 0x90, maybeSize);//初始化为nop指令
		::VirtualProtect(buf, maybeSize, PAGE_EXECUTE_READWRITE, &dwOldProtect);
		//本进程是32位的 用32位的函数,64位改下调用函数
		auto  bincode = AsmkeyStoneDll::Assemble32(_asmcode, (DWORD64)buf);
		::WriteProcessMemory(::GetCurrentProcess(), buf, bincode.data(), bincode.size(), 0);
		return (DWORD64)buf;
	}
}