#pragma once

#define  WIN32_LEAN_AND_MEAN
#include <windows.h>

struct ks_struct;
typedef struct ks_struct ks_engine;

// Keystone API version
#define KS_API_MAJOR 0
#define KS_API_MINOR 9

// Package version
#define KS_VERSION_MAJOR KS_API_MAJOR
#define KS_VERSION_MINOR KS_API_MINOR
#define KS_VERSION_EXTRA 2

/*
  Macro to create combined version which can be compared to
  result of ks_version() API.
*/
#define KS_MAKE_VERSION(major, minor) ((major << 8) + minor)

// Architecture type
typedef enum ks_arch {
	KS_ARCH_ARM = 1,    // ARM architecture (including Thumb, Thumb-2)
	KS_ARCH_ARM64,      // ARM-64, also called AArch64
	KS_ARCH_MIPS,       // Mips architecture
	KS_ARCH_X86,        // X86 architecture (including x86 & x86-64)
	KS_ARCH_PPC,        // PowerPC architecture (currently unsupported)
	KS_ARCH_SPARC,      // Sparc architecture
	KS_ARCH_SYSTEMZ,    // SystemZ architecture (S390X)
	KS_ARCH_HEXAGON,    // Hexagon architecture
	KS_ARCH_EVM,        // Ethereum Virtual Machine architecture
	KS_ARCH_MAX,
} ks_arch;

// Mode type
typedef enum ks_mode {
	KS_MODE_LITTLE_ENDIAN = 0,    // little-endian mode (default mode)
	KS_MODE_BIG_ENDIAN = 1 << 30, // big-endian mode
	// arm / arm64
	KS_MODE_ARM = 1 << 0,              // ARM mode
	KS_MODE_THUMB = 1 << 4,       // THUMB mode (including Thumb-2)
	KS_MODE_V8 = 1 << 6,          // ARMv8 A32 encodings for ARM
	// mips
	KS_MODE_MICRO = 1 << 4,       // MicroMips mode
	KS_MODE_MIPS3 = 1 << 5,       // Mips III ISA
	KS_MODE_MIPS32R6 = 1 << 6,    // Mips32r6 ISA
	KS_MODE_MIPS32 = 1 << 2,      // Mips32 ISA
	KS_MODE_MIPS64 = 1 << 3,      // Mips64 ISA
	// x86 / x64
	KS_MODE_16 = 1 << 1,          // 16-bit mode
	KS_MODE_32 = 1 << 2,          // 32-bit mode
	KS_MODE_64 = 1 << 3,          // 64-bit mode
	// ppc 
	KS_MODE_PPC32 = 1 << 2,       // 32-bit mode
	KS_MODE_PPC64 = 1 << 3,       // 64-bit mode
	KS_MODE_QPX = 1 << 4,         // Quad Processing eXtensions mode
	// sparc
	KS_MODE_SPARC32 = 1 << 2,     // 32-bit mode
	KS_MODE_SPARC64 = 1 << 3,     // 64-bit mode
	KS_MODE_V9 = 1 << 4,          // SparcV9 mode
} ks_mode;

// All generic errors related to input assembly >= KS_ERR_ASM
#define KS_ERR_ASM 128

// All architecture-specific errors related to input assembly >= KS_ERR_ASM_ARCH
#define KS_ERR_ASM_ARCH 512

// All type of errors encountered by Keystone API.
typedef enum ks_err {
	KS_ERR_OK = 0,   // No error: everything was fine
	KS_ERR_NOMEM,      // Out-Of-Memory error: ks_open(), ks_emulate()
	KS_ERR_ARCH,     // Unsupported architecture: ks_open()
	KS_ERR_HANDLE,   // Invalid handle
	KS_ERR_MODE,     // Invalid/unsupported mode: ks_open()
	KS_ERR_VERSION,  // Unsupported version (bindings)
	KS_ERR_OPT_INVALID,  // Unsupported option

	// generic input assembly errors - parser specific
	KS_ERR_ASM_EXPR_TOKEN = KS_ERR_ASM,    // unknown token in expression
	KS_ERR_ASM_DIRECTIVE_VALUE_RANGE,   // literal value out of range for directive
	KS_ERR_ASM_DIRECTIVE_ID,    // expected identifier in directive
	KS_ERR_ASM_DIRECTIVE_TOKEN, // unexpected token in directive
	KS_ERR_ASM_DIRECTIVE_STR,   // expected string in directive
	KS_ERR_ASM_DIRECTIVE_COMMA, // expected comma in directive
	KS_ERR_ASM_DIRECTIVE_RELOC_NAME, // expected relocation name in directive
	KS_ERR_ASM_DIRECTIVE_RELOC_TOKEN, // unexpected token in .reloc directive
	KS_ERR_ASM_DIRECTIVE_FPOINT,    // invalid floating point in directive
	KS_ERR_ASM_DIRECTIVE_UNKNOWN,    // unknown directive
	KS_ERR_ASM_DIRECTIVE_EQU,   // invalid equal directive
	KS_ERR_ASM_DIRECTIVE_INVALID,   // (generic) invalid directive
	KS_ERR_ASM_VARIANT_INVALID, // invalid variant
	KS_ERR_ASM_EXPR_BRACKET,    // brackets expression not supported on this target
	KS_ERR_ASM_SYMBOL_MODIFIER, // unexpected symbol modifier following '@'
	KS_ERR_ASM_SYMBOL_REDEFINED, // invalid symbol redefinition
	KS_ERR_ASM_SYMBOL_MISSING,  // cannot find a symbol
	KS_ERR_ASM_RPAREN,          // expected ')' in parentheses expression
	KS_ERR_ASM_STAT_TOKEN,      // unexpected token at start of statement
	KS_ERR_ASM_UNSUPPORTED,     // unsupported token yet
	KS_ERR_ASM_MACRO_TOKEN,     // unexpected token in macro instantiation
	KS_ERR_ASM_MACRO_PAREN,     // unbalanced parentheses in macro argument
	KS_ERR_ASM_MACRO_EQU,       // expected '=' after formal parameter identifier
	KS_ERR_ASM_MACRO_ARGS,      // too many positional arguments
	KS_ERR_ASM_MACRO_LEVELS_EXCEED, // macros cannot be nested more than 20 levels deep
	KS_ERR_ASM_MACRO_STR,    // invalid macro string
	KS_ERR_ASM_MACRO_INVALID,    // invalid macro (generic error)
	KS_ERR_ASM_ESC_BACKSLASH,   // unexpected backslash at end of escaped string
	KS_ERR_ASM_ESC_OCTAL,       // invalid octal escape sequence  (out of range)
	KS_ERR_ASM_ESC_SEQUENCE,         // invalid escape sequence (unrecognized character)
	KS_ERR_ASM_ESC_STR,         // broken escape string
	KS_ERR_ASM_TOKEN_INVALID,   // invalid token
	KS_ERR_ASM_INSN_UNSUPPORTED,   // this instruction is unsupported in this mode
	KS_ERR_ASM_FIXUP_INVALID,   // invalid fixup
	KS_ERR_ASM_LABEL_INVALID,   // invalid label
	KS_ERR_ASM_FRAGMENT_INVALID,   // invalid fragment

	// generic input assembly errors - architecture specific
	KS_ERR_ASM_INVALIDOPERAND = KS_ERR_ASM_ARCH,
	KS_ERR_ASM_MISSINGFEATURE,
	KS_ERR_ASM_MNEMONICFAIL,
} ks_err;

// Resolver callback to provide value for a missing symbol in @symbol.
// To handle a symbol, the resolver must put value of the symbol in @value,
// then returns True.
// If we do not resolve a missing symbol, this function must return False.
// In that case, ks_asm() would eventually return with error KS_ERR_ASM_SYMBOL_MISSING.

// To register the resolver, pass its function address to ks_option(), using
// option KS_OPT_SYM_RESOLVER. For example, see samples/sample.c.
typedef bool (*ks_sym_resolver)(const char* symbol, uint64_t* value);

// Runtime option for the Keystone engine
typedef enum ks_opt_type {
	KS_OPT_SYNTAX = 1,    // Choose syntax for input assembly
	KS_OPT_SYM_RESOLVER,  // Set symbol resolver callback
} ks_opt_type;


// Runtime option value (associated with ks_opt_type above)
typedef enum ks_opt_value {
	KS_OPT_SYNTAX_INTEL = 1 << 0, // X86 Intel syntax - default on X86 (KS_OPT_SYNTAX).
	KS_OPT_SYNTAX_ATT = 1 << 1, // X86 ATT asm syntax (KS_OPT_SYNTAX).
	KS_OPT_SYNTAX_NASM = 1 << 2, // X86 Nasm syntax (KS_OPT_SYNTAX).
	KS_OPT_SYNTAX_MASM = 1 << 3, // X86 Masm syntax (KS_OPT_SYNTAX) - unsupported yet.
	KS_OPT_SYNTAX_GAS = 1 << 4, // X86 GNU GAS syntax (KS_OPT_SYNTAX).
	KS_OPT_SYNTAX_RADIX16 = 1 << 5, // All immediates are in hex format (i.e 12 is 0x12)
} ks_opt_value;


#include "arm64.h"
#include "arm.h"
#include "evm.h"
#include "hexagon.h"
#include "mips.h"
#include "ppc.h"
#include "sparc.h"
#include "systemz.h"
#include "x86.h"

typedef unsigned int(_cdecl* _ks_version)(unsigned int* major, unsigned int* minor);

typedef bool(_cdecl* _ks_arch_supported)(ks_arch arch);
typedef ks_err(_cdecl* _ks_open)(ks_arch arch, int mode, ks_engine** ks);
typedef ks_err(_cdecl* _ks_close)(ks_engine* ks);
typedef ks_err(_cdecl* _ks_errno)(ks_engine* ks);
typedef const char* (_cdecl* _ks_strerror)(ks_err code);
typedef ks_err(_cdecl* _ks_option)(ks_engine* ks, ks_opt_type type, size_t value);
typedef int(_cdecl* _ks_asm)(ks_engine* ks,
	const char* string,
	uint64_t address,
	unsigned char** encoding, size_t* encoding_size,
	size_t* stat_count);
typedef void(_cdecl* _ks_free)(unsigned char* p);
/*
#include "MyFile.h"
#include "MemoryLoadClass.h"
using namespace std;
class MemoryKeyStoneAPI
{
public:
	MemoryLoadClass memload;
	_ks_version ks_version;
	_ks_arch_supported ks_arch_supported;
	_ks_open ks_open;
	_ks_close ks_close;
	_ks_errno ks_errno;
	_ks_strerror ks_strerror;
	_ks_option ks_option;
	_ks_asm ks_asm;
	_ks_free ks_free;
	~MemoryKeyStoneAPI()
	{

	}
	MemoryKeyStoneAPI()
	{
		_ks_version ks_version = nullptr;
		_ks_arch_supported ks_arch_supported = nullptr;
		_ks_open ks_open = nullptr;
		_ks_close ks_close = nullptr;
		_ks_errno ks_errno = nullptr;
		_ks_strerror ks_strerror = nullptr;
		_ks_option ks_option = nullptr;
		_ks_asm ks_asm = nullptr;
		_ks_free ks_free = nullptr;
	}

	void Init(const char* FilePath = "./keystone.dll") {
		std::stringstream tmp=MyFile::Reader::ReadAllFile(FilePath);
		memload.LoadDll(tmp.str().data());
		ks_version = (decltype(ks_version))memload.MemoryGetProcAddress("ks_version");
		ks_arch_supported = (decltype(ks_arch_supported))memload.MemoryGetProcAddress("ks_arch_supported");
		ks_open = (decltype(ks_open))memload.MemoryGetProcAddress("ks_open");
		ks_close = (decltype(ks_close))memload.MemoryGetProcAddress("ks_close");
		ks_errno = (decltype(ks_errno))memload.MemoryGetProcAddress("ks_errno");
		ks_strerror = (decltype(ks_strerror))memload.MemoryGetProcAddress("ks_strerror");
		ks_option = (decltype(ks_option))memload.MemoryGetProcAddress("ks_option");
		ks_asm = (decltype(ks_asm))memload.MemoryGetProcAddress("ks_asm");
		ks_free = (decltype(ks_free))memload.MemoryGetProcAddress("ks_free");
		if (!ks_option || !ks_arch_supported || !ks_asm || !ks_open)
		{
			FatalAppExit(-1,"MemoryKeyStoneAPI Init Error\n");
		}
		printf("addressTest£º%x\n", ks_asm);
	}
};*/