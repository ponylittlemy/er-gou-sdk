#pragma once
using namespace std;


/*
在C++中，头文件中的函数定义不需要写extern关键字。因为在头文件中定义的函数默认是inline函数，
即在编译时会将函数体直接插入到调用该函数的地方，而不是在链接时进行函数的链接。因此，不需要使用extern关键字来告诉编译器该函数是在其他文件中定义的。
需要注意的是，如果在头文件中定义的函数不是inline函数，而是普通的函数，那么需要在函数定义前加上extern关键字，
表示该函数是在其他文件中定义的。否则，在链接时会出现重复定义的错误。


如果在头文件中声明了一个inline函数，但是在源文件中没有提供该函数的定义，
那么编译器会在链接时报错，提示找不到该函数的定义。因此，inline函数的定义以头文件中的声明为准，而不是以源文件中的声明为准。
*/

namespace MySdk {
	//转换
	extern memory::string<memory::new_allocator> memoryVectorToString(memory::vector<UCHAR, memory::new_allocator>& dt);;//不要反回引用
	extern std::string  VectorToString(std::vector<UCHAR>& dt);
	//按键状态
	extern inline bool ISKEYDOWN(DWORD vk_code);
	extern inline bool ISKEYUP(DWORD vk_code);
	//CRC
	extern inline unsigned int  crc32_code(const  char* buf, unsigned int size);
	
	extern inline void us_Delay(ULONG ulMicroSeconds);
	//随机数
	extern   int RtlRandomEx(int a, int b);//WIN32版本
	template<typename T>
	T RandomCpp(T a, T b) {//C++版本
		static std::uniform_int_distribution<T> u(a, b);
		static std::default_random_engine e;    // 生成无符号随机整数
		return u(e);
	}
	//弹框
	extern void ErrorOut(const char* fmt, ...);
	extern 	void ErrorOutExit(const char* fmt, ...);
	extern DWORD ErrorOutExitYesNo(const char* fmt, ...);//返回值:IDYES或者IDNO

	extern int MessageBoxTimeoutA(HWND hWnd, LPCSTR lpText,
		LPCSTR lpCaption, UINT uType, WORD wLanguageId,
		DWORD dwMilliseconds);
	template <typename... ARG>
	void MessaageSuperTime(int tm,const char* tmp, ARG... args) {
		string re = FFormat(tmp, args...);
		MessageBoxTimeoutA(0, re.c_str(), "提示", 0, 0, tm);
	}


	//启动操作
	extern void 删除开机启动();
	extern void 添加开机启动(std::string& path);
	extern bool 重启机器();
	//数组操作
	template <typename A, typename... ARG> static inline A MaxCalc(ARG... args) {//计算数组中的最大值
		std::vector<A> Arrays = { (A)args... };
		return *std::max_element(Arrays.begin(), Arrays.end());
	}

	template <typename A>
	void shuffle(A& a) { //数组随机排序
		auto get_URBG = []() {
			// 使用random_device生成seed
			std::mt19937 g(std::random_device{}());

			return g;
		};

		return std::shuffle(std::begin(a), std::end(a), get_URBG());
	}
	template <typename A> //通用清空容器
	static void Clear(A& a) {
		if (a.size() != 0) {
			A b;
			std::swap(b, a);
		}
	}



}


namespace MySdk::http {
	extern std::optional<std::string> HttpDownLoadFile(std::string& ip, int port, std::string& url,
		bool fixByRoot = true,
		bool urlEncode = false);
	extern void  SendMiaoMaMessage(string& str, string& miaoma);//微信 公众号喵码提醒


	namespace TxtCoding { //编码转换 HTTPLIB这个detail里面还有一大堆功能函数 比如BASE64
		inline std::string encode_url(const std::string& s);
		inline std::string decode_url(const std::string& s,
			bool convert_plus_to_space);
		inline void GB2312ToUTF_8(string& pOut, char* pText, int pLen);
		inline void Gb2312ToUnicode(WCHAR* pOut, char* gbBuffer);
		inline void UTF_8ToUnicode(WCHAR* pOut, char* pText);
		inline void UnicodeToUTF_8(char* pOut, WCHAR* pText);
	}
}

namespace MySdk::Code_Encry {
	// protuBuf ZigZag编码 32位
// https://blog.csdn.net/daaikuaichuan/article/details/105639884
// https://blog.csdn.net/daaikuaichuan/article/details/105639884
	extern int int_to_zigzag(int n);
	extern int zigzag_to_int(int n);
	extern int64_t int64_to_zigzag(int64_t n);
	extern int64_t zigzag_to_int64(int64_t n);
	extern void __fastcall xor_encrypt(BYTE* pbData, int nSize);
	extern void __fastcall xor_decrypt(BYTE* pbData, int nSize);

	template <typename T> inline T __rol(T val, size_t count) {
		size_t bitcount = sizeof(T) * 8;

		count %= bitcount;
		return (val << count) | (val >> (bitcount - count));
	}

	template <typename T> inline T __ror(T val, size_t count) {
		size_t bitcount = sizeof(T) * 8;

		count %= bitcount;
		return (val >> count) | (val << (bitcount - count));
	}
}
namespace stringConv {

	extern inline std::string Win32_Utf8ToAnsi(const std::string utf8);
	extern inline std::string Win32_Utf8ToAnsi_2(const std::string utf8);
	extern inline std::string Win32_Utf8ToAnsi(const char* utf8);

	extern inline std::string Win32_AnsiToUtf8(const char* gb2312);
	extern inline std::string Win32_AnsiToUtf8(const std::string gb2312);
	extern inline std::string Win32_AnsiToUtf8_2(const std::string gb2312);

	extern inline std::wstring Win32_StringToWstring(std::string& strascii);
	extern inline std::string Win32_WstringToString(const std::wstring& widestring);
}
namespace Str {
	// 40 00 4E 08 格式字符串转BYTE数组
	extern std::vector<BYTE> stringToHexVector(std::string str);
	//空格作为分隔符 trim_empty 设置false否则出错
	extern std::vector<std::string> SplitByGetLine(const std::string& str, const char seq,
		const bool trim_empty = true);
	extern std::vector<std::string> SplitByGetLine(const std::string& str,
		const bool trim_empty = true);
	extern std::vector<std::string> SplitBySpace(const std::string& str);
	extern std::vector<std::string> Compact(const std::vector<std::string>& tokens);
	extern std::vector<std::string> Split2(const std::string& str,
		const std::string& delim,
		const bool trim_empty = false);
	extern std::string Join(const std::vector<std::string>& tokens,
		const std::string& delim, const bool trim_empty = false);
	extern std::string Trim(const std::string& str);
	extern std::string Repeat(const std::string& str, unsigned int times);
	extern std::string ToUpper(const std::string& str);
	extern std::string ToLower(const std::string& str);
	extern std::string ReadFile(const std::string& filepath);
	extern void WriteFile(const std::string& filepath, const std::string& content);

	template <typename... Args>
	int get_formatSize(const char* format, Args... args) {
		//获取格式化后的长度
		int length = std::snprintf(nullptr, 0, format, args...);
		return length;
	}

	template <typename... Args>
	std::string string_sprintf(const char* format, Args... args) {
		//获取格式化后的长度
		int length = std::snprintf(nullptr, 0, format, args...);
		if (length < 0) {
			FatalAppExit(-1, "string_sprintf error");
		}
		//申请长度
		char* buf = new char[length + 1];
		std::snprintf(buf, length + 1, format, args...);

		std::string str(buf);
		delete[] buf;
		return str;
	}

	extern ATL::CAtlString StringToCstring(std::string const& s);
	//转换 传入char*  转换成UTF8 char*字符串
	extern std::string CStringToUTF8String(ATL::CAtlString cstr, int& len);
	//转换过程：先将utf8转双字节Unicode编码，再通过WideCharToMultiByte将宽字符转换为多字节。
	//------------------------------------------------------
	extern std::wstring string2wstring(std::string str);
	//将wstring转换成string
	extern std::string wstring2string(std::wstring wstr);
	extern std::string WcharToChar(const wchar_t* wp, size_t encode);
	extern bool isNum(std::string str);
	std::string GetHex(PBYTE Data, ULONG dwBytes);
	//
	extern void ReplaceAll(std::string& strSource, const std::string strOld,
		const std::string strNew);
	extern void split(const string src, const string separator, vector<string>& dest);
	extern vector<BYTE> split_HexByte(const string src, const string separator);
	extern vector<DWORD> split_int(const string src, const string separator);
	extern memory::vector<int, memory::new_allocator>  stringToVecInt(const std::string str);
	extern vector<float> split_float(const string src, const string separator);
	extern vector<string> split_string(const string src, const string separator);

	extern bool str_includeBySplit(const std::string str, const std::string msg,
		const string separator, bool same);
	extern bool str_include(const std::string str, const std::string msg, bool same);
	extern bool str_include_IC(const std::string str, const std::string msg,
		bool same = false);
	extern bool StrInVector(vector<string> vec, string str, bool same = true);
	extern bool StrInVector(string str, vector<string> vec, bool same = true);

	extern std::string FillString(string src, int len, std::string ch);
	extern std::string FillString_Left(string src, int len, std::string ch);

}; // namespace Str

//Str的很多年久失修 ,Str2是一些替代
namespace Str2 {
	//是否包含 str1包含str2
	extern inline bool isContainAB(const std::string& str1, const std::string& str2);
	extern inline bool isContainABPassCase(const std::string& str1, const std::string& str2);//不区分大小写 中文的话就别用了
	//isContainAB_kmp据说执行快 亲测在编译器优化的情况下 根本不如isContainAB
	extern inline bool isContainAB_kmp(const std::string& text, const std::string& pattern);
	//是否包含 str1包含str2 或 str2	包含str1
	extern inline bool isContainABBA(const std::string& str1, const std::string& str2);
	extern inline bool isContainABBAPassCase(const std::string& str1, const std::string& str2);//不区分大小写 中文的话就别用了
	//是否完全一样
	extern inline bool isSame(const std::string& str1, const std::string& str2);
	extern inline bool isSamePassCase(const std::string& str1, const std::string& str2);//不区分大小写 中文的话就别用了
	extern bool isStartWith(const std::string& str, const std::string& prefix);
	extern bool isStartWithPassCase(const std::string& str, const std::string& prefix);//不区分大小写 中文的话就别用了


}



//控制台管理
namespace ConsoleConfig {

	extern inline void  colorRed();
	extern  inline void  colorWhite();

	extern inline void  colorFen();
	extern inline void  colorQing();

	extern inline void  colorGreen();
	extern inline void  colorYellow();


	extern inline void  trace();
	extern inline void  debug();
	extern inline void  info();
	extern inline void  warn();
	extern inline	void  err();
	extern inline	void  critical();
	extern inline void  setsatae(int _showState);

	extern bool isshow;
};


//计算时间用的而已
namespace CodeRunTimer {
	extern inline void start();
	extern inline void  end(const char*);
	extern inline double elapsed();
}

//进程和窗口枚举
namespace WindowProcessTool {
	extern inline std::vector<PROCESSENTRY32> EmunProcess();
	//auto [OK,hw, pid,tid] = WindowProcessTool::GetWindowByProcessName("soul");
	extern inline std::tuple<bool, HWND, DWORD, DWORD> GetWindowByProcessName(const char* Name);//Ok, hw,pid, tid
	extern inline std::tuple<bool, HWND, DWORD, DWORD> GetWindowByProcessID(const char* title, DWORD  PID);


	extern inline std::string GetProcessNameByProcessID(DWORD  PID);
	extern inline std::tuple<bool, HWND, DWORD, DWORD> GetProcessByTittleAndProcessName(const char* TitleName, const char* ProcessName);
	extern inline std::tuple<bool, HWND, DWORD, DWORD> GetProcessByClassNameAndProcessId(const char* className, DWORD pid);
	extern inline std::tuple<bool, HWND, DWORD, DWORD>  GetProcessByClassName(const char* className,bool passCase=false);
	extern inline std::tuple<bool, HWND, DWORD, DWORD> GetProcessByTittleName(const char* TitleName);
	extern inline std::tuple<bool, HWND, DWORD, DWORD>  GetProcessByTittleAndClassName(const char* TitleName, const char* Classname);
	extern inline DWORD  FindProcessByName(const char* Name);
	extern inline bool  EnableDebugPrivilege();
}



namespace MyFileApi{
	extern std::stringstream readFileByStl(std::string FilePath);//速度第二
	extern memory::vector<BYTE, memory::new_allocator> readBigFile(std::string fileName);//速度第一 返回的是内存池里面的字节数组
	extern memory::vector<UCHAR, memory::new_allocator> readFileByApi(std::string FilePath);//速度第三 返回的是内存池里面的字节数组


	extern memory::string<memory::new_allocator> readFileStringByApi(std::string FilePath);//不要反回引用
	extern memory::string<memory::new_allocator> readBigStringFile(std::string fileName);//不要反回引用
	extern std::string readFileStringByStl(std::string FilePath);

	extern int getFileSize(std::string FilePath);
	extern int getFileSizeByApi(std::string FilePath);
	extern int getFileSizeByApi(HANDLE fileHandle);
	extern std::tuple<bool, HANDLE> openFileByApi(std::string FilePath, bool canWrite=false);
	
	//写
	extern bool  writeFileStl(string file, BYTE* data, int len,bool appendMode=false);
	extern bool  writeFileStringStl(string file, std::string s, bool appendMode = false);
	template <typename... ARG>
	void writeFileFmtStl(string file, bool appendMode, const char* tmp, ARG... args) {
		string re = FFormat(tmp, args...);
		writeFileStringStl(file, re, appendMode);
	}
	//写 API
	extern bool writeFileApi(string file, BYTE* data, int len, bool appendMode=false);
	extern bool writeFileStringApi(string file, string s, bool appendMode = false);
	template <typename... ARG>
	void writeFileFmtApi(string file, bool appendMode, const char* tmp, ARG... args) {
		string re = FFormat(tmp, args...);
		writeFileStringApi(file, re, appendMode);
	}

} // namespace MyFileReader




//读写接口,兼容32位的  传入4字节有效地址就是32位
using mem_Fuc = std::function<bool(
	int sz, DWORD64 adress, void* bf)>; //接口函数内部最好读取失败清空缓冲区


class MyMem64 {
	//多了个firstAddress必须写是因为怕粗心忘记传地址
#define AddReadFunc(Fn) template <typename... ARG> inline Fn Read##Fn(DWORD64 firstAddress, ARG... args) {\
		return ReadType<Fn>(firstAddress,std::forward(args...));\
	}

#define AddWriteFunc(Fn) template <typename... ARG> inline void Write##Fn(Fn data,DWORD64 firstAddress,ARG... args) {\
		 WriteType<Fn>(data,firstAddress,std::forward(args...));\
	}
public:
	mem_Fuc rd_fn = nullptr;
	mem_Fuc wt_fn = nullptr;
	void init(mem_Fuc fn, mem_Fuc fn_wt) {
		rd_fn = fn;
		wt_fn = fn_wt;
	}
	inline void checkErr() {
		static bool bcheck = false;
		if (bcheck) {
			return;
		}
		bcheck = true;
		if (rd_fn == nullptr || wt_fn == nullptr) {
			MySdk::ErrorOutExit("读写函数未初始化");
		};
	};
	
	MyMem64(void) {
		rd_fn = nullptr;
		wt_fn = nullptr;
	};
	~MyMem64(void) {};
	//////////////////////////////////////////////////////////////////////////
	template <typename... ARG>
	memory::vector<UCHAR, memory::new_allocator>
		ReadMemVector(int size, QWORD firstAddress, ARG... args) {
		/*if (sizeof...(args) == 0)
		{
				Helper::ErrorOut("ReadMemVector函数 size是0");
				DebugBreak();
		}*/
		memory::vector<UCHAR, memory::new_allocator> arrret;
		// vector<UCHAR> arrret;
		arrret.resize(size);
		ReadBySize(size, arrret.data(), firstAddress, std::forward(args...));
		return arrret;
	}


	template <typename... ARG> std::string ReadString(int size, ARG... args) {
		auto xx = ReadMemVector(size, std::forward(args...));
		return MySdk::VectorToString(xx);
	}
	template <typename... ARG> std::string ReadUtf8String(int size, ARG... args) {
		auto xx = ReadMemVector(size, std::forward(args...));
		memory::string<memory::new_allocator> s= MySdk::memoryVectorToString(xx);
		return stringConv::Win32_Utf8ToAnsi(s);
	}
	//修改建议:https://zhuanlan.zhihu.com/p/437524195
	template <typename... ARG>
	inline bool ReadBySize(int sz, void* bf, ARG... args) {
		/*if (sizeof...(args) == 0)
		{
				DebugBreak();
				Helper::ErrorOut("ReadBySize函数 size是0");
		}*/
		bool bsucess;
		vector<DWORD64> Arrays = { (DWORD64)args... };
		DWORD64 Tmp, Tmp2;
		Tmp = 0;
		for (int i = 0; i < ((int)Arrays.size() - 1); i++) {
			Tmp2 = Tmp + Arrays[i];
			bsucess = InlineRead(8, &Tmp, Tmp2);
			if (!bsucess) {
				FPrintf("ReadBySize READ ERR\n");
				memset(bf, 0, sz);
				return false;
			}
		}
		Tmp2 = Tmp + Arrays.at(Arrays.size() - 1);
		return InlineRead(sz, bf, Tmp2);
	}

	template <typename ValueType, typename... ARG>
	inline ValueType ReadType(ARG... args) {
		ValueType ret;
		memset(&ret, 0, sizeof(ret));
		ReadBySize(sizeof(ret), &ret, std::forward(args...));
		return ret;
	}



	
	//写入
	template <typename... ARG>
	inline void WriteBySize(int sz, void* bf, DWORD64 addrwess1, ARG... args) {
		bool bsucess;
		vector<DWORD64> Arrays = { addrwess1,(DWORD64)args... };
		DWORD64 Tmp, Tmp2;
		Tmp = 0;
		for (int i = 0; i < (int)(Arrays.size() - 1); i++) {
			Tmp2 = Tmp + Arrays[i];
			bsucess = InlineRead(8, &Tmp, Tmp2);
			if (!bsucess) {
				memset(bf, 0, sz);
				return;
			}
		}
		Tmp2 = Tmp + Arrays[Arrays.size() - 1];
		return InlineWrite(sz, bf, Tmp2);
	}

	template <typename ValueType, typename... ARG>
	inline void WriteType(ValueType data, DWORD64 addrwess1, ARG... args) {
		ValueType ret = data;
		WriteBySize(sizeof(ValueType), &ret, addrwess1, std::forward(args...));
	}
	//常用数据类型

	AddReadFunc(DWORD);
	AddReadFunc(DWORD64);
	AddReadFunc(BYTE);
	AddReadFunc(float);
	AddReadFunc(double);
	AddReadFunc(POINT);

	AddWriteFunc(BYTE);
	AddWriteFunc(DWORD);
	AddWriteFunc(DWORD64);
	AddWriteFunc(float);
	AddWriteFunc(double);
	AddWriteFunc(POINT);


	template <typename... ARG> inline bool ReadCan(ARG... args) {
		DWORD64 ret = 0;
		return ReadBySize(8, &ret, std::forward(args...));
	}


	template <typename T>
	static T GetVectorData(memory::vector<UCHAR, memory::new_allocator>& vec,
		DWORD64 off, string debugCode) {
		// FPrintf("size:{},off+sizeof(T):{}\n", vec.size(), off + sizeof(T));
		if (vec.size() < ((int)off + sizeof(T))) {
			/*	Check_SetDataErr(true);
					return T{0};*/
			MySdk::ErrorOut(
				"GetVectorData 越界:size:%x off:%x off+Sizeof(T):%x \n 调试信息是:%s",
				vec.size(), (int)off, (int)off + sizeof(T), debugCode.c_str());
			::DebugBreak();
		}
		return *(T*)(vec.data() + (int)off);
	}

	template <typename T>
	static T GetVectorData(vector<UCHAR>& vec, DWORD64 off, string debugCode) {
		// FPrintf("size:{},off+sizeof(T):{}\n", vec.size(), off + sizeof(T));
		if (vec.size() < ((int)off + sizeof(T))) {
			/*	Check_SetDataErr(true);
					return T{0};*/
			MySdk::ErrorOut(
				"GetVectorData 越界:size:%x off:%x off+Sizeof(T):%x \n 调试信息是:%s",
				vec.size(), (int)off, (int)off + sizeof(T), debugCode.c_str());
			::DebugBreak();
		}
		return *(T*)(vec.data() + (int)off);
	}
private:
	inline bool InlineRead(int sz, void* buf, DWORD64 adr) //地址失效 卡顿引起
	{
		if (adr < 0x10000) {
			printf("ERRRO ADR:%llx \r\n", adr);
			memset(buf, 0, sz);
			return false;
		}
		checkErr();
		auto bsucess = rd_fn(sz, adr, buf);
		if (!bsucess) {
			memset(buf, 0, sz);
		}
		return bsucess;
	}

	inline void InlineWrite(int sz, void* buf, DWORD64 adr) {
		checkErr();
		wt_fn(sz, adr, buf);
	}
};




class MyTimer {
public:
	MyTimer(bool uslastTime = true) {
		restart();
		if (uslastTime) {
			lasttime -= 60 * 1000 * 60; // 60分钟
		}
	}
	bool elapsedAndRestart(int ms) {
		if (elapsed() >= ms) {
			restart();
			return true;
		}
		return false;
	}
	void restart() //重置计时器
	{
		lasttime = ::GetTickCount();
	}

	int elapsed() //计算时差
	{
		return ::GetTickCount() - lasttime;
	}

private:
	DWORD lasttime;
};

class LockSleep { //限制一个函数每秒钟最多只调用指定的次数
public:
	LockSleep(int fps) : m_fps(fps) { m_clock = ::GetTickCount(); };
	void FpsLock() {
		int curtime = GetTickCount();
		int off = curtime - m_clock;
		int fpsTime = (int)(1000.0f / m_fps);
		if (off < fpsTime) // off<66
		{
			FPrintf("FPS SLEEP:{}\n", fpsTime - off);
			Sleep(fpsTime - off);
			curtime = GetTickCount();
		}
		m_clock = curtime;
	}

private:
	int m_fps;
	int m_clock;
};



namespace PathApi {
	extern void recordAppPath();//记录程序的路径 用作有隐藏进程的时候,GetCurExePathName是可能不管用的 隐藏前调用
	//当前全路径包含EXE  Q:\NewClass\Release\NewClass.exe
	extern std::string getRecordAppExeFullPath();
	extern std::string getRecordAppExeName();
	extern std::string getRecordAppDir();
	extern void fixAppCurPath();//修复当前路径 recordAppPath调用后可以用,vs调试模式运行 需要修复,否则当前路径可能在源码目录,也可以通过编译器设置来修复


	extern void setCurRentPath(string ph);
	extern inline string getCurExePathName();//获取当前exe 的全路径
	extern inline string getCurExePath();//获取当前exe的目录
	extern inline string getDirByFullPath(string phName);//根据全路径提取所在目录
	extern inline string getDirByFullPath2(string phName);//根据全路径提取所在目录2
	extern inline string getFileNameByFullPath(string phName);//根据全路径提取其文件名
	extern inline string getFileExtByFullPath(string phName);//根据全路径提取其文件名扩展名  .exe  .txt  .dll

	extern vector<string> getDirFiles(string dir, string  ext="*");//枚举目录中的文件  不包含子目录  传入  ./testdir 或 ./testdir/ 都行  ,扩展名不写或则传入.txt  .exe这种
	extern vector<string> getDirFilesEx(string dir, string  ext = "*");//枚举目录中的文件  包含子目录  传入  ./testdir 或 ./testdir/ 都行,扩展名不写或则传入.txt  .exe这种


	extern string relativePathToAbsPath(string pt);//相对路径转绝对路径
	extern string absPathToRelativePath(string pt);//绝对路径转相对路径

	//判断文件或者目录是否存在传入相对或者绝对的都行
	extern bool Exist(string fileOrPath);
	//目录拷贝 不会拷贝子目录
	extern bool CopyEx(string f, string t);
	extern bool Delete(string f);//删目录或者文件
	extern void  Create(string pt);//创建支持多级目录的文件夹  如果传人的参数有文件名 也会当做目录生成 "./testCreate/A/B/C/D/E/F/1.txt":1.txt会是个文件夹名称
}



class PakStreamDump {
public:
	template <typename T> static std::stringstream PAK(T const& mp) {
		std::stringstream ss;
		msgpack::pack(ss, mp);
		return ss;
	}
	template <typename T> static T UnPAK(const char* buf, int len) {
		auto obj = msgpack::unpack(buf, len);
		auto vv = obj.get().as<T>();
		return vv;
	}
};

//内存加载库
class MemoryLoadClass {
public:
	MemoryLoadClass() { m_Moudle = nullptr; }
	void* m_Moudle;
	bool LoadDll(const void* buffer);
	DWORD MemoryGetProcAddress(const char* fn);
	void MemoryFreeLibrary();
	bool isLoad() { return m_Moudle != nullptr; };
};
//内存加载keyStone汇编引擎
namespace AsmkeyStoneDll {
	extern void Init(std::string pt);
	extern void InitFromMemoty(const void* buf);
	extern std::vector<BYTE> Assemble32(std::string p_sASM, DWORD adresss);
	extern std::vector<BYTE> Assemble64(std::string p_sASM, DWORD64 adresss);
};


//内存加载CapStone汇编引擎
namespace AsmCapStone {
	/*extern void Init(std::string pt);
	extern void InitFromMemoty(const void* buf);*/
	extern std::string  Capsttone_diasm64(DWORD64 code, int size, DWORD64 baseadresss, int strSize = 1000);
	extern std::string  Capsttone_diasm64_real(DWORD64 code, int size, DWORD64 baseadresss, int strSize = 1000);//反回不带额外信息的反汇编
	extern std::string  Capsttone_diasm32_real(DWORD64 code, int size, DWORD64 baseadresss, int strSize = 1000);//反回不带额外信息的反汇编
	//获取指定汇编行的[寄存器+偏移]里面的数据 常用作特征码定位 []里面的数据不支持绝对地址 比如0x7fff51112 支持偏移类型 比如 [rcx+0x123] 提取出来是0x123
	extern std::tuple<bool,DWORD64,DWORD64> Capsttone_GetAsmDispAddress64(DWORD64 codeAddress, int size, DWORD64 baseadresss, int asmLine);//[是否成功,定位到的地址,得出的数据]
	//同上  是单独的支持 [RIP]的那种 比如  lea rbx,QWORD PTR DS:[0x7fff5214]  对 mov rbx,QWORD PTR DS:[0x7fff5214] 似乎是算法需要改进的
	extern std::tuple<bool, DWORD64, DWORD64> Capsttone_GetAsmDispAddress64_FixRip(DWORD64 codeAddress, int size, DWORD64 baseadresss, int asmLine);
	//取得立即数
	extern std::tuple<bool, DWORD64, DWORD64>  Capsttone_GetAsmImmAddress64(DWORD64 codeAddress, int size, DWORD64 baseadresss, int asmLine);
};

namespace AsmExt {
	//测试 可能不稳定 给定的size太大可能会出错 在无效内存范围,需要自己深入挖掘了
	extern DWORD64  乾坤大挪移(DWORD64 src, int maybeSize);
}