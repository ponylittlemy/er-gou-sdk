﻿
// myServer.cpp: 定义应用程序的类行为。
//

#include "pch.h"
#include "framework.h"
#include "myServer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMyServerApp

BEGIN_MESSAGE_MAP(CMyServerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CMyServerApp 构造

CMyServerApp::CMyServerApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
}


// 唯一的 CMyServerApp 对象

CMyServerApp theApp;


// CMyServerApp 初始化
/*

BOOL CMyServerApp::InitInstance()
{
	CWinApp::InitInstance();


	// 创建 shell 管理器，以防对话框包含
	// 任何 shell 树视图控件或 shell 列表视图控件。
	CShellManager *pShellManager = new CShellManager;

	// 激活“Windows Native”视觉管理器，以便在 MFC 控件中启用主题
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	CmyServerDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此放置处理何时用
		//  “确定”来关闭对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用
		//  “取消”来关闭对话框的代码
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "警告: 对话框创建失败，应用程序将意外终止。\n");
		TRACE(traceAppMsg, 0, "警告: 如果您在对话框上使用 MFC 控件，则无法 #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS。\n");
	}

	// 删除上面创建的 shell 管理器。
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}
*/
/*
#include <msclr\auto_gcroot.h>
#include <Microsoft.CodeAnalysis.CSharp.h>
#include <System.Reflection.h>*/
void  TestAsm();
BOOL CMyServerApp::InitInstance()
{
	CWinApp::InitInstance();
	ConsoleConfig::setsatae(1);
	Str::WriteFile("test.txt", "1232322");
	for (int i = 0; i < 10; i++) {
		MyFileApi::writeFileStringApi("./testWriteApi_append.txt", "Hello", true);
	}
	for (int i = 0; i < 10; i++) {
		MyFileApi::writeFileStringApi("./testWriteApi.txt", "Hello", false);
	}
	for (int i = 0; i < 10; i++) {
		MyFileApi::writeFileFmtStl("./testWrite.txt", true, "index:{}", i);
	}
	for (int i = 0; i < 100; i++) {
		FPrintf("{}:{},", MySdk::RtlRandomEx(0, 100), MySdk::RandomCpp(0, 100));
	}
	FPrintf("\r\n");
	PathApi::recordAppPath();
	PathApi::fixAppCurPath();
	FPrintf("getRecordAppDir:{}\n", PathApi::getRecordAppDir());
	FPrintf("getFileExtByFullPath:{}\n", PathApi::getFileExtByFullPath("./TEST/123.exe"));
	FPrintf("Read:{}\n", MyFileApi::readFileStringByStl("./testread.txt"));
	FPrintf("A:{}\n", Str2::isStartWith("二狗啊", "二狗"));
	FPrintf("A:{}\n", Str2::isContainAB_kmp("a二狗啊", "二狗"));
	FPrintf("A:{}\n", Str2::isContainAB_kmp("二狗", "二狗啊"));
	FPrintf("A:{}\n", Str2::isContainAB("我当时的", "我当时的"));
	FPrintf("A:{}\n", Str2::isContainAB("我当时的", "当时的"));
	FPrintf("isSamePassCase:{}\n", Str2::isSamePassCase("AAAA", "aaaa"));
	FPrintf("isSame:{}\n", Str2::isSame("AAAA", "aaaa"));
	FPrintf("A:{}\n", Str2::isSame("我当时的", "当时的"));
	FPrintf("A:{}\n", Str2::isSame("撒旦士大夫", "撒旦士大夫"));
	FPrintf("A:{} {}\n", PathApi::getCurExePath(), PathApi::getCurExePathName());
	FPrintf("A:{}\n", string("顶顶顶啊我啊顶").find_last_of("啊我啊"));
	FPrintf("A:{}\n", string("顶顶顶啊我啊顶").find_last_of("的"));
	auto [OK, hw, pid, tid] = WindowProcessTool::GetWindowByProcessName("soul");
	FPrintf("ok:{} hw: {} tid: {} pid:{}\n", OK, (DWORD)hw, tid, pid);

	if (1) {//使用msgpack官方的例子
		msgpack::type::tuple<int, bool, std::string> src(1, true, "example");

		// serialize the object into the buffer.
		// any classes that implements write(const char*,size_t) can be a buffer.
		std::stringstream buffer;
		msgpack::pack(buffer, src);

		// send the buffer ...
		buffer.seekg(0);

		// deserialize the buffer into msgpack::object instance.
		std::string str(buffer.str());

		msgpack::object_handle oh = msgpack::unpack(str.data(), str.size());

		// deserialized object is valid during the msgpack::object_handle instance alive.
		msgpack::object deserialized = oh.get();

		// msgpack::object supports ostream.
		std::cout << deserialized << std::endl;
	}
	if (1) {//使用我自己封装的
		std::tuple<int, bool, std::string> src(26, false, "ergou");
		auto s =PakStreamDump::PAK(src);//打包
		//解包后用解构语法
		auto [a,b,c] = PakStreamDump::UnPAK<std::tuple<int, bool, std::string>>(s.str().c_str(), (int)s.str().length());
		std::cout << a<<","<<b << "," << c << std::endl;
	}
	if (1) {
		auto v=PathApi::getDirFilesEx("./testdir",".txt");
		for (auto&x:v) {
			FPrintf("FILE:{} 父目录:{} 绝对路径:{}\n", x, PathApi::getDirByFullPath2(x),PathApi::relativePathToAbsPath(x));
		}
	}
	PathApi::CopyEx("./testDir", "./dirCopy");
	PathApi::CopyEx("./testDir", "./dirCopy2");
	PathApi::Delete("./dircopy2/1.txt");
	PathApi::Delete("./dircopy");
	PathApi::Create("./testCreate/A/B/C/D/E/F/1.txt");
	TestAsm();
	MySdk::ErrorOut("OK!");


	getchar();
	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}


void  TestQQQ();
void  TestAsm()
{
	AsmkeyStoneDll::InitFromMemoty(MyFileApi::readFileByApi("./memoryDll/keystone.dll").data());
	//AsmCapStoneDll::InitFromMemoty(MyFileApi::readFileByApi("./memoryDll/capstone.dll").data());
	if (1) {//内存加载DLL 正常情况下这个文件是服务器读取或者从加密的文本读取 才会有好处
		
		auto  asmcode = AsmkeyStoneDll::Assemble64("mov rax,0x123 \n push 0x2 \n mov rcx,[rax+0x568] \n lea rbx,QWORD PTR DS:[0x7fff5214]\n jmp 0x40404040 \n call 0x07ff00040", 0x4000000);//2个nop方便识别
		FPrintf("Assemble64:{}\n", asmcode);//用fmt打印汇编字节数组

		auto  asmret = AsmCapStone::Capsttone_diasm64((DWORD64)asmcode.data(), asmcode.size(), 0x4000000);
		FPrintf("Capsttone_diasm64:\r\n {}\n", asmret);//查看反汇编结果


		{
			auto [OK, ADR, Value] = AsmCapStone::Capsttone_GetAsmDispAddress64((DWORD64)asmcode.data(), asmcode.size(), 0x4000000, 3);
			FPrintf("定位到的数据:ok{},ADR:{:X} VAR:{:X}\n", OK, ADR, Value);
		}

		{
			auto [OK, ADR, Value] = AsmCapStone::Capsttone_GetAsmDispAddress64_FixRip((DWORD64)asmcode.data(), asmcode.size(), 0x4000000, 4);
			FPrintf("基址类型 定位到的数据:ok{},ADR:{:X} VAR:{:X}\n", OK, ADR, Value);
		}

		{
			auto [OK, ADR, Value] = AsmCapStone::Capsttone_GetAsmImmAddress64((DWORD64)asmcode.data(), asmcode.size(), 0x4000000, 5);
			FPrintf("立即数类型 定位到的数据:ok{},ADR:{:X} VAR:{:X}\n", OK, ADR, Value);
		}
		{
			auto [OK, ADR, Value] = AsmCapStone::Capsttone_GetAsmImmAddress64((DWORD64)asmcode.data(), asmcode.size(), 0x4000000, 6);
			FPrintf("立即数类型 定位到的数据:ok{},ADR:{:X} VAR:{:X}\n", OK, ADR, Value);
		}
	}


	DWORD64 movFunc = AsmExt::乾坤大挪移((DWORD64)TestQQQ, 48);
	typedef void(*callFn)();
	callFn fn = (callFn)(DWORD)movFunc;
	fn();

}



void  TestQQQ() {
	AfxMessageBox("調用到了");
}